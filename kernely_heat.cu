/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#include <stdio.h>
#include <cuda_runtime.h>

#include "kernely_heat.h"
#include "defines.h"

// deklarace kernulu pro spousteci funkce runKernel_heat_Vxxx
__global__ void kernel_heat_V1(float *inputD, float *outputD);
__global__ void kernel_heat_V2_2(float *inputD, float *outputD);
__global__ void kernel_heat_V3(float *inputD, float *outputD);
__global__ void kernel_heat_V4_1(float *inputD, float *outputD);
__global__ void kernel_heat_V5(float *inputD, float *outputD);
__global__ void kernel_heat_V5_1(float *inputD, float *outputD);

//  textura s parametrem prostředí Beta
texture<float,3,cudaReadModeElementType> tex_beta;

cudaArray *prepareTexture(float *betaH,int dimx, int dimy, int dimz)
{

    cudaExtent extent;
    // prvni parametr nesmi byt dimx*sizeof(float), v dokumentaci je to BLBE
    // http://developer.download.nvidia.com/compute/cuda/4_1/rel/toolkit/docs/online/group__CUDART__MEMORY_g6ce5d637817b7693f6c2601aef21a294.html
    extent=make_cudaExtent(dimx,dimy,dimz);

    cudaChannelFormatDesc channel;
    channel=cudaCreateChannelDesc<float>();

    cudaArray* cudaarray;
    if (cudaMalloc3DArray(&cudaarray,&channel,extent) == cudaErrorMemoryAllocation)
    {
        cudaError_t cudaErr = cudaGetLastError();
        printf("chyba alokace %s\n", cudaGetErrorString(cudaErr));
    }

    // parametry pro kopirovani z hostu do karty
    cudaMemcpy3DParms memcpyParms={0};
    memcpyParms.kind = cudaMemcpyHostToDevice;
    memcpyParms.extent = extent;
    memcpyParms.dstArray = cudaarray;
    memcpyParms.srcPtr = make_cudaPitchedPtr( (void*)betaH,sizeof(float)*dimx,dimx,dimy);
    cudaMemcpy3D(&memcpyParms); // samotne kopirovani

    // nastaveni textury
    // Mode Clamp kdyz se presahnou souradnice textury, tak se vratim k nejblizsi souradnici
    // tady se ani nevyuziva
    tex_beta.addressMode[0]=cudaAddressModeClamp;
    tex_beta.addressMode[1]=cudaAddressModeClamp;
    tex_beta.addressMode[2]=cudaAddressModeClamp;
    tex_beta.filterMode=cudaFilterModePoint;        // bez interpolace
    tex_beta.normalized = false;                    // souradnice nejsou v intervalu <0-1>

    // navazeme texturu na cudaArray
    cudaBindTextureToArray(tex_beta,cudaarray,channel);

    return cudaarray;
}

void freeTexture(cudaArray *cudaarray)
{
    cudaUnbindTexture(tex_beta);
    cudaFreeArray(cudaarray);
}


//=================================================
//                   KERNEL V1 - naivni
//=================================================

// zadne vyuziti sdilene pameti...
// matice je rozdelena do krychlicek 8x8x8, kde se provede
// vstupni matice 260x260x260 [Z,Y,X]

#define TOTAL_WIDTH 260
#define BLOCK_WIDTH 8

void runKernel_heat_V1(float *inputD, float *outputD)
{
    // dimenze gridu a bloku
    dim3 dimGrid(32,32,32);
    dim3 dimBlock(8,8,8);

    // preferujeme L1 cache na ukor shared memory
    cudaFuncSetCacheConfig(kernel_heat_V1,cudaFuncCachePreferL1);

    kernel_heat_V1 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_heat_V1(float *inputD, float *outputD)
{  
    // souradnice ve vstupni matici, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = (((blockIdx.z*BLOCK_WIDTH)+2)*TOTAL_WIDTH*TOTAL_WIDTH) + // osa z
            (((blockIdx.y*BLOCK_WIDTH)+2)*TOTAL_WIDTH) + // osa y
            ((blockIdx.x*BLOCK_WIDTH)+2);    // osa x

    // promenne pro okoli bodu
    float beta, t_akt, x_p1,x_p2,x_m1,x_m2, y_p1,y_p2,y_m1,y_m2, z_p1,z_p2,z_m1,z_m2;

    float xindex = ((float)(threadIdx.x + (blockIdx.x*BLOCK_WIDTH)))  ;
    float yindex = ((float)(threadIdx.y + (blockIdx.y*BLOCK_WIDTH)))  ;
    float zindex = ((float)(threadIdx.z + (blockIdx.z*BLOCK_WIDTH)))  ;

    // pozice threadu v vstupni matici
    int aktualni_pozice = pozice_blok + (threadIdx.z*TOTAL_WIDTH*TOTAL_WIDTH) + (threadIdx.y*TOTAL_WIDTH) + threadIdx.x;

    // beta
    beta = tex3D(tex_beta,xindex,yindex,zindex);

    // serazeno podle pravdepodobnosti ze se nachazi v L1 cache
    t_akt =   inputD[aktualni_pozice];

    z_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH*TOTAL_WIDTH];
    z_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH*TOTAL_WIDTH];

    y_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH];
    y_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH];

    x_m1 =    inputD[aktualni_pozice - 1];
    x_m2 =    inputD[aktualni_pozice - 2];

    x_p1 =  inputD[aktualni_pozice + 1];
    x_p2 =  inputD[aktualni_pozice + 2];

    y_p1 =  inputD[aktualni_pozice + TOTAL_WIDTH];
    y_p2 =  inputD[aktualni_pozice + 2*TOTAL_WIDTH];

    z_p1 =  inputD[aktualni_pozice + TOTAL_WIDTH*TOTAL_WIDTH];
    z_p2 =  inputD[aktualni_pozice + 2*TOTAL_WIDTH*TOTAL_WIDTH];

    outputD[aktualni_pozice] =
            (t_akt + DELTA_T*beta*(
                 ((z_m2 -16*z_m1 + 30*t_akt - 16*z_p1 + z_p2) * DELTA_Z) +
                 ((y_m2 -16*y_m1 + 30*t_akt - 16*y_p1 + y_p2) * DELTA_Y) +
                 ((x_m2 -16*x_m1 + 30*t_akt - 16*x_p1 + x_p2) * DELTA_X)
                 )
             );


}

//=================================================
//                   KERNEL V2_2
//=================================================

// vyuziva se sdilena pamet
// blok 8x8x8 threadu
//
// vstupni matice 260x260x272, v ose x je z kazde strany 6 sloupcu navic kvuli zarovnani globalni pameti
// (sekce 1,2,3,4,E,F viz nacrtek v kernelu je 32b)
//
// krychlicka sdilene pameti ma ted 12x12x40 a pocita 4 male 8x8x8 krychlicky v ose x
// teoreticky by stacilo 12x12x36, ale je pridano dalsich 4, aby se vyhnulo shared memory banks conflicts
// viz banks_conflict_V2_2.xlsx
//
// optimalizace nacitani, kazda deska (stejna osa z) nacita jednu stenu krychlicky
// => uz nestoji vetsina threadu ve warpu, nacitaji bud vsechny thready ve warpu nebo zadny

void runKernel_heat_V2_2(float *inputD, float *outputD)
{
    dim3 dimGrid(8,32,32);
    dim3 dimBlock(8,8,8);

    kernel_heat_V2_2 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

#undef TOTAL_WIDTH
#undef TOTAL_WIDTH_X
#undef TOTAL_WIDTH_Y
#undef BLOCK_WIDTH
#define TOTAL_WIDTH_X 272
#define TOTAL_WIDTH_Y 260
#define BLOCK_WIDTH 8

__global__ void kernel_heat_V2_2(float *inputD, float *outputD)
{   

    int tz = threadIdx.z;
    int ty = threadIdx.y;
    int tx = threadIdx.x;

    int tz_plus2 = tz+2;
    int ty_plus2 = ty+2;
    int tx_plus2 = tx+2;

    // stacilo by [12][12][36] ale je tam 40, aby se predeslo shared memory banks conflict,
    __shared__ float krychlicka[12][12][40];

    // souradnice ve vstupni matici, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    // (vsude +2 protoze je okraj o velikosti 1 ve vsech dimenzich)
    // + 6 kvuli zarovnani globalni pameti, v ose x je z kazde strany 6 sloupcu navic
    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH+2)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            ((blockIdx.y*BLOCK_WIDTH+2)*TOTAL_WIDTH_X) + // osa y
            (blockIdx.x*(BLOCK_WIDTH*4)+2) + 6;    // osa x, 6 kvuli zarovnani pameti

    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (tz*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (ty*TOTAL_WIDTH_X) +tx;


    /* pohled zhora v ose z na 1 blok
     *         E E E E
     *        _E_E_E_E_
     *    A A |1|2|3|4| B B
     *         F F F F
     *         F F F F
     */

    // kazdy thread nacita svou aktualni pozici, postupne 4x pro vsechny 4 krychlicky
    krychlicka[tz_plus2][ty_plus2][tx_plus2] = inputD[aktualni_pozice];  // samotna krychlicka 1
    krychlicka[tz_plus2][ty_plus2][tx_plus2+BLOCK_WIDTH] = inputD[aktualni_pozice+BLOCK_WIDTH]; // samotna krychlicka 2
    krychlicka[tz_plus2][ty_plus2][tx_plus2+2*BLOCK_WIDTH] = inputD[aktualni_pozice+2*BLOCK_WIDTH]; // samotna krychlicka 3
    krychlicka[tz_plus2][ty_plus2][tx_plus2+3*BLOCK_WIDTH] = inputD[aktualni_pozice+3*BLOCK_WIDTH]; // samotna krychlicka 4


    // nacteni hranic

    // okraje A
    if (tz == 0)
    {
        krychlicka[ty_plus2][tx_plus2][0] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) -2]; // x==0
        krychlicka[ty_plus2][tx_plus2][1] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) -1]; // x==1
    }
    else
    {
        // okraje B
        if (tz == 1)
        {
            krychlicka[ty_plus2][tx_plus2][BLOCK_WIDTH+2 + 3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) + BLOCK_WIDTH + 3*BLOCK_WIDTH]; //x==max // okraj C
            krychlicka[ty_plus2][tx_plus2][BLOCK_WIDTH+3 + 3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) + BLOCK_WIDTH+1 + 3*BLOCK_WIDTH]; //x==max // okraj C
        }
        else
        {
            if (tz==4) // okraj E
            {
                // y==0
                krychlicka[ty_plus2][0][tx_plus2] =               inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx -2*TOTAL_WIDTH_X ];
                krychlicka[ty_plus2][0][tx_plus2+BLOCK_WIDTH] =   inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx -2*TOTAL_WIDTH_X + BLOCK_WIDTH];
                krychlicka[ty_plus2][0][tx_plus2+2*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx -2*TOTAL_WIDTH_X + 2*BLOCK_WIDTH];
                krychlicka[ty_plus2][0][tx_plus2+3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx -2*TOTAL_WIDTH_X + 3*BLOCK_WIDTH];

                // y==1
                krychlicka[ty_plus2][1][tx_plus2] =               inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X ];
                krychlicka[ty_plus2][1][tx_plus2+BLOCK_WIDTH] =   inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X + BLOCK_WIDTH];
                krychlicka[ty_plus2][1][tx_plus2+2*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X + 2*BLOCK_WIDTH];
                krychlicka[ty_plus2][1][tx_plus2+3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X + 3*BLOCK_WIDTH];
            }
            else
            {
                if (tz==5) // okraj F
                {
                    // y==max-1
                    krychlicka[ty_plus2][BLOCK_WIDTH+2][tx_plus2] =               inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH)*TOTAL_WIDTH_X) + tx ]; // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+2][tx_plus2+BLOCK_WIDTH] =   inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH)*TOTAL_WIDTH_X) + tx + BLOCK_WIDTH]; // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+2][tx_plus2+2*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH)*TOTAL_WIDTH_X) + tx + 2*BLOCK_WIDTH]; // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+2][tx_plus2+3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH)*TOTAL_WIDTH_X) + tx + 3*BLOCK_WIDTH]; // y==max

                    // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+3][tx_plus2] =               inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx ]; // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+3][tx_plus2+BLOCK_WIDTH] =   inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx + BLOCK_WIDTH]; // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+3][tx_plus2+2*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx + 2*BLOCK_WIDTH]; // y==max
                    krychlicka[ty_plus2][BLOCK_WIDTH+3][tx_plus2+3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx + 3*BLOCK_WIDTH]; // y==max
                }
                else
                {
                    if (tz==6) // dolni podstava
                    {
                        // z==0
                        krychlicka[0][ty_plus2][tx_plus2] =               inputD[aktualni_pozice -8*TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
                        krychlicka[0][ty_plus2][tx_plus2+BLOCK_WIDTH] =   inputD[aktualni_pozice -8*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + BLOCK_WIDTH];
                        krychlicka[0][ty_plus2][tx_plus2+2*BLOCK_WIDTH] = inputD[aktualni_pozice -8*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 2*BLOCK_WIDTH];
                        krychlicka[0][ty_plus2][tx_plus2+3*BLOCK_WIDTH] = inputD[aktualni_pozice -8*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 3*BLOCK_WIDTH];

                        // z==1
                        krychlicka[1][ty_plus2][tx_plus2] =               inputD[aktualni_pozice -7*TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
                        krychlicka[1][ty_plus2][tx_plus2+BLOCK_WIDTH] =   inputD[aktualni_pozice -7*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + BLOCK_WIDTH];
                        krychlicka[1][ty_plus2][tx_plus2+2*BLOCK_WIDTH] = inputD[aktualni_pozice -7*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 2*BLOCK_WIDTH];
                        krychlicka[1][ty_plus2][tx_plus2+3*BLOCK_WIDTH] = inputD[aktualni_pozice -7*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 3*BLOCK_WIDTH];
                    }
                    else
                    {
                        if (tz==7) // horni podstava
                        {
                            // z==max-1
                            krychlicka[BLOCK_WIDTH+2][ty_plus2][tx_plus2] =               inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
                            krychlicka[BLOCK_WIDTH+2][ty_plus2][tx_plus2+BLOCK_WIDTH] =   inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y + BLOCK_WIDTH];
                            krychlicka[BLOCK_WIDTH+2][ty_plus2][tx_plus2+2*BLOCK_WIDTH] = inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 2*BLOCK_WIDTH];
                            krychlicka[BLOCK_WIDTH+2][ty_plus2][tx_plus2+3*BLOCK_WIDTH] = inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 3*BLOCK_WIDTH];

                            // z==max
                            krychlicka[BLOCK_WIDTH+3][ty_plus2][tx_plus2] =               inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
                            krychlicka[BLOCK_WIDTH+3][ty_plus2][tx_plus2+BLOCK_WIDTH] =   inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + BLOCK_WIDTH];
                            krychlicka[BLOCK_WIDTH+3][ty_plus2][tx_plus2+2*BLOCK_WIDTH] = inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 2*BLOCK_WIDTH];
                            krychlicka[BLOCK_WIDTH+3][ty_plus2][tx_plus2+3*BLOCK_WIDTH] = inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 3*BLOCK_WIDTH];

                        }
                    }
                }
            }
        }
    }

    __syncthreads();

    // indexy do textury
    float xindex = ((float)(threadIdx.x + (blockIdx.x*BLOCK_WIDTH*4)))  ;
    float yindex = ((float)(threadIdx.y + (blockIdx.y*BLOCK_WIDTH)))  ;
    float zindex = ((float)(threadIdx.z + (blockIdx.z*BLOCK_WIDTH))) ;
    float beta;
    float buf;
    float t_akt;


    // zapis krychlicky 1
    t_akt = krychlicka[tz_plus2][ty_plus2][tx_plus2];
    beta = tex3D(tex_beta,xindex,yindex,zindex);

    buf =  ((krychlicka[tz_plus2-2][ty_plus2][tx_plus2] - 16*krychlicka[tz_plus2-1][ty_plus2][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2+1][ty_plus2][tx_plus2] + krychlicka[tz_plus2+2][ty_plus2][tx_plus2]) * DELTA_Z);
    buf += ((krychlicka[tz_plus2][ty_plus2-2][tx_plus2] - 16*krychlicka[tz_plus2][ty_plus2-1][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2+1][tx_plus2] + krychlicka[tz_plus2][ty_plus2+2][tx_plus2]) * DELTA_Y);
    buf += ((krychlicka[tz_plus2][ty_plus2][tx_plus2-2] - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2-1] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2+1] + krychlicka[tz_plus2][ty_plus2][tx_plus2+2]) * DELTA_X);
    outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

    tx_plus2 += BLOCK_WIDTH;
    xindex += (float)BLOCK_WIDTH;
    aktualni_pozice += BLOCK_WIDTH;

    // zapis krychlicky 2
    t_akt = krychlicka[tz_plus2][ty_plus2][tx_plus2];
    beta = tex3D(tex_beta,xindex,yindex,zindex);
    buf =  ((krychlicka[tz_plus2-2][ty_plus2][tx_plus2] - 16*krychlicka[tz_plus2-1][ty_plus2][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2+1][ty_plus2][tx_plus2] + krychlicka[tz_plus2+2][ty_plus2][tx_plus2]) * DELTA_Z);
    buf += ((krychlicka[tz_plus2][ty_plus2-2][tx_plus2] - 16*krychlicka[tz_plus2][ty_plus2-1][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2+1][tx_plus2] + krychlicka[tz_plus2][ty_plus2+2][tx_plus2]) * DELTA_Y);
    buf += ((krychlicka[tz_plus2][ty_plus2][tx_plus2-2] - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2-1] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2+1] + krychlicka[tz_plus2][ty_plus2][tx_plus2+2]) * DELTA_X);
    outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

    tx_plus2 += BLOCK_WIDTH;
    xindex += (float)BLOCK_WIDTH;
    aktualni_pozice += BLOCK_WIDTH;

    // zapis krychlicky 3
    t_akt = krychlicka[tz_plus2][ty_plus2][tx_plus2];
    beta = tex3D(tex_beta,xindex,yindex,zindex);
    buf =  ((krychlicka[tz_plus2-2][ty_plus2][tx_plus2] - 16*krychlicka[tz_plus2-1][ty_plus2][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2+1][ty_plus2][tx_plus2] + krychlicka[tz_plus2+2][ty_plus2][tx_plus2]) * DELTA_Z);
    buf += ((krychlicka[tz_plus2][ty_plus2-2][tx_plus2] - 16*krychlicka[tz_plus2][ty_plus2-1][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2+1][tx_plus2] + krychlicka[tz_plus2][ty_plus2+2][tx_plus2]) * DELTA_Y);
    buf += ((krychlicka[tz_plus2][ty_plus2][tx_plus2-2] - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2-1] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2+1] + krychlicka[tz_plus2][ty_plus2][tx_plus2+2]) * DELTA_X);
    outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

    tx_plus2 += BLOCK_WIDTH;
    xindex += (float)BLOCK_WIDTH;
    aktualni_pozice += BLOCK_WIDTH;

    // zapis krychlicky 4
    t_akt = krychlicka[tz_plus2][ty_plus2][tx_plus2];
    beta = tex3D(tex_beta,xindex,yindex,zindex);
    buf =  ((krychlicka[tz_plus2-2][ty_plus2][tx_plus2] - 16*krychlicka[tz_plus2-1][ty_plus2][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2+1][ty_plus2][tx_plus2] + krychlicka[tz_plus2+2][ty_plus2][tx_plus2]) * DELTA_Z);
    buf += ((krychlicka[tz_plus2][ty_plus2-2][tx_plus2] - 16*krychlicka[tz_plus2][ty_plus2-1][tx_plus2] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2+1][tx_plus2] + krychlicka[tz_plus2][ty_plus2+2][tx_plus2]) * DELTA_Y);
    buf += ((krychlicka[tz_plus2][ty_plus2][tx_plus2-2] - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2-1] + 30*t_akt - 16*krychlicka[tz_plus2][ty_plus2][tx_plus2+1] + krychlicka[tz_plus2][ty_plus2][tx_plus2+2]) * DELTA_X);
    outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

}


//=================================================
//                   KERNEL V3
//=================================================

// 16x16x1 threadu, nactou ve for cyklu vsech 16 pater krychle (12x12x12 pocitanych bodu i okraje)
// 12x12x1 threadu spocita prumery uvnitr krychle = 112 z 256 threadu stoji
// velikost vstupni matice 268x268x268 (2 body z kazde strany okraje)
// velikost beta matice 264x264x264

// 1blok = 512threadu, 16KB shared memory => presne 3 bloky na 1SM (max 1536threadu, 48KB sh. mem)

#undef TOTAL_WIDTH_X
#undef TOTAL_WIDTH_Y
#undef BLOCK_WIDTH_Y
#undef BLOCK_WIDTH_Y
#undef BLOCK_WIDTH_Z

#undef TOTAL_WIDTH
#define TOTAL_WIDTH 268
#undef BLOCK_WIDTH
#define BLOCK_WIDTH 12
#define HALF_BLOCK_WIDTH 6

void runKernel_heat_V3(float *inputD, float *outputD)
{
    dim3 dimGrid(22,22,22);
    dim3 dimBlock(16,16,1);

    kernel_heat_V3 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_heat_V3(float *inputD, float *outputD)
{

    int ty = threadIdx.y;
    int tx = threadIdx.x;

    __shared__ float krychlicka[16][16][16];

    // souradnice ve vstupni matici, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH)*TOTAL_WIDTH*TOTAL_WIDTH) + // osa z
            ((blockIdx.y*BLOCK_WIDTH)*TOTAL_WIDTH) + // osa y
            (blockIdx.x*BLOCK_WIDTH);    // osa x

    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH) +tx;;

    // cyklus pres vsechny patra krychlicky
    for (signed int i=0; i< BLOCK_WIDTH+4; i++)
    {
        // kazdy thread nacita svoje
        krychlicka[i][ty][tx] = inputD[aktualni_pozice];

        // posun v ose Z o 2 nahoru
        aktualni_pozice += TOTAL_WIDTH*TOTAL_WIDTH;
    }

    __syncthreads();

    // vynechame okraje
    if ((ty>1) && (ty<(BLOCK_WIDTH+2)) && (tx>1) && (tx<(BLOCK_WIDTH+2)))
    {

        // indexy do textury
        float xindex = ((float)(tx -2 + (blockIdx.x*BLOCK_WIDTH)))  ;
        float yindex = ((float)(ty -2 + (blockIdx.y*BLOCK_WIDTH))) ;
        float zindex = ((float)(blockIdx.z*BLOCK_WIDTH)) ;
        float beta;
        float buf;
        float t_akt;

        // vynechame dolni podstavu
        aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH) +tx  + 2*TOTAL_WIDTH*TOTAL_WIDTH;

        // for cyklus pres vsechny dlazdice
        #pragma unroll 12
        for (signed int i=2; i< (BLOCK_WIDTH+2); i++)
        {
            t_akt = krychlicka[i][ty][tx];
            beta = tex3D(tex_beta,xindex,yindex,zindex);
            buf =  ((krychlicka[i-2][ty][tx] - 16*krychlicka[i-1][ty][tx] + 30*t_akt - 16*krychlicka[i+1][ty][tx] + krychlicka[i+2][ty][tx]) * DELTA_Z);
            buf += ((krychlicka[i][ty-2][tx] - 16*krychlicka[i][ty-1][tx] + 30*t_akt - 16*krychlicka[i][ty+1][tx] + krychlicka[i][ty+2][tx]) * DELTA_Y);
            buf += ((krychlicka[i][ty][tx-2] - 16*krychlicka[i][ty][tx-1] + 30*t_akt - 16*krychlicka[i][ty][tx+1] + krychlicka[i][ty][tx+2]) * DELTA_X);

            outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

            // posun v ose Z o 2 nahoru
            aktualni_pozice += TOTAL_WIDTH*TOTAL_WIDTH;
            zindex += 1.0f;
        }

    }
}

//=================================================
//                   KERNEL V4_1
//=================================================


// hybridni kernel
// 32x8 threadu postupne nactou 12(8 + 4 okraj) pater krychlicky
// 28x8 threadu potom spocita prumer
// 2 thready navic jsou pouze pro nacteni okraju v ose X
//  => pri vypoctu samotnem je tedy divergence threadu (4 z 32 stoji), ale lepsi nez sh. memory banks conflicts a serializace pristupu
// okraje v ose Y se nactou prevracenim 30x8 threadu

// velikost vstupni matice 260x260x284, beta matice 256x256x280
// volatile -0,25s
// vlezou se presne 2 bloky na SM, 1blok=768threadu a 18KB shared memory

// nacitani je rozdeleno do 3 pater v ose Z, kazda dela cast
// z==0 pocitane body v patre 1-4, dolni okraje v ose Z, k tomu i prislusne okraje v ose x
// z==1 to same pro patra 5-8 a horni okraje osy Z
// z==2 okraje v ose Y

// vypocet se rozdeli na patra z==0 -> prvni 3, z==1 druhe 3, z==2 posledni 2

#undef TOTAL_WIDTH_X
#undef BLOCK_WIDTH

#undef TOTAL_WIDTH_X
#define TOTAL_WIDTH_X 284
#undef TOTAL_WIDTH_Y
#define TOTAL_WIDTH_Y 260
#undef BLOCK_WIDTH_X
#define BLOCK_WIDTH_X 28
#undef BLOCK_WIDTH_Y
#define BLOCK_WIDTH_Y 8
#undef BLOCK_WIDTH_Z
#define BLOCK_WIDTH_Z 8

void runKernel_heat_V4_1(float *inputD, float *outputD)
{
    dim3 dimGrid(10,32,32);
    dim3 dimBlock(32,8,3);

    kernel_heat_V4_1 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_heat_V4_1(float *inputD, float *outputD)
{
    int ty = threadIdx.y;
    int tx = threadIdx.x;
    int tz = threadIdx.z;

    __shared__ float krychlicka[12][12][32];

    // souradnice ve vstupni matici, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = (((blockIdx.z*BLOCK_WIDTH_Z))*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            (((blockIdx.y*BLOCK_WIDTH_Y)+2)*TOTAL_WIDTH_X) + // osa y
            (blockIdx.x*BLOCK_WIDTH_X);    // osa x

    int aktualni_pozice;

    // nacteni samotne krychlicky + dolni(i==0 i==1) podstavy
    // pocitane body v patre 1-4 osy Z, dolni okraje v ose Z, k tomu vsemu i prislusne okraje v ose x
    // okraje pro X==0, X==1 a X=Max-1 X==Max jsou reseny ze se zde nacita rovnou 32 bodu, ale prumer pocitame pro 28!
    if (tz==0)
    {
        aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx;
        for (signed int i=0; i< 6; i++)
        {
            // kazdy thread nacita svoje
            krychlicka[i][ty+2][tx] = inputD[aktualni_pozice];

            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
        }
    }

    // nacteni samotne krychlicky + horni(i==10 i==11)  podstavy
    // pocitane body v patre 5-8 osy Z, horni okraje v ose Z, k tomu vsemu i prislusne okraje v ose x
    // okraje pro X==0, X==1 a X=Max-1 X==Max jsou reseny ze se zde nacita rovnou 32 bodu, ale prumer pocitame pro 28!
    else if (tz==1)
    {
        aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx + 6*TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
        for (signed int i=6; i< (BLOCK_WIDTH_Z+4); i++)
        {
            // kazdy thread nacita svoje
            krychlicka[i][ty+2][tx] = inputD[aktualni_pozice];

            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
        }
    }

    // okraje v ose Y
    else if ((tx>1) && (tx<BLOCK_WIDTH_X+2))
    {
        // nacteni okraje Y==0, prohodi se osy Y a Z
        aktualni_pozice = pozice_blok + ((ty+2)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) -2*TOTAL_WIDTH_X +tx;
        krychlicka[ty+2][0][tx] = inputD[aktualni_pozice];

        // nacteni okraje Y==1, prohodi se osy Y a Z
        aktualni_pozice = pozice_blok + ((ty+2)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) - TOTAL_WIDTH_X +tx;
        krychlicka[ty+2][1][tx] = inputD[aktualni_pozice];

        // nacteni okraje Y==BLOCK_WIDTH_Y+2, prohodi se osy Y a Z
        aktualni_pozice = pozice_blok + ((ty+2)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH_Y)*TOTAL_WIDTH_X) +tx;
        krychlicka[ty+2][BLOCK_WIDTH_Y+2][tx] = inputD[aktualni_pozice];

        // nacteni okraje Y==BLOCK_WIDTH_Y+3, prohodi se osy Y a Z
        aktualni_pozice = pozice_blok + ((ty+2)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH_Y+1)*TOTAL_WIDTH_X) +tx;
        krychlicka[ty+2][BLOCK_WIDTH_Y+3][tx] = inputD[aktualni_pozice];
    }

    __syncthreads();
    /*
     * debugovaci kod - vypise obsah krychlicky v urcite threadu a blocku
     *
     **/
    /*
    if ( (blockIdx.x==0) && (blockIdx.y==0) && (blockIdx.z==0) && (tz==0) && (ty==0) && (tx==0))
    {
        for (int k=0; k<10 ;k++)
        {
            for (int j=0; j<10 ;j++)
            {
                for (int i=0; i<10 ;i++)
                {
                    printf("%f\t",krychlicka[k][j][i]);
                }
                printf("\n");
            }
            printf("--------\n");
        }
        printf("-------------------------------------------------------------------------\n");
    }
*/


    // samotny vypocet

    if (tz==0)
    {
        if ((tx>1) && (tx<BLOCK_WIDTH_X+2))
        {
            float xindex = ((float)(threadIdx.x -2.0f + (blockIdx.x*BLOCK_WIDTH_X))) ;
            float yindex = ((float)(threadIdx.y + (blockIdx.y*BLOCK_WIDTH_Y)))  ;
            float zindex = ((float)((blockIdx.z*BLOCK_WIDTH_Z))) ;
            float beta;
            float buf;
            float t_akt;

            // vynechame dolni podstavu
            aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx  + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
            ty+=2;

            // for cyklus pres osu Z
            //#pragma unroll 4
            for (signed int i=2; i< 5; i++)
            {
                beta = tex3D(tex_beta,xindex,yindex,zindex);
                t_akt = krychlicka[i][ty][tx];

                buf =  ((krychlicka[i-2][ty][tx] - 16*krychlicka[i-1][ty][tx] + 30*t_akt - 16*krychlicka[i+1][ty][tx] + krychlicka[i+2][ty][tx]) * DELTA_Z);
                buf += ((krychlicka[i][ty-2][tx] - 16*krychlicka[i][ty-1][tx] + 30*t_akt - 16*krychlicka[i][ty+1][tx] + krychlicka[i][ty+2][tx]) * DELTA_Y);
                buf += ((krychlicka[i][ty][tx-2] - 16*krychlicka[i][ty][tx-1] + 30*t_akt - 16*krychlicka[i][ty][tx+1] + krychlicka[i][ty][tx+2]) * DELTA_X);

                outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

                // posun v ose Z o 1 nahoru
                aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
                zindex += 1.0f;
            }
        }
    }
    else if (tz==1)
    {
        if ((tx>1) && (tx<BLOCK_WIDTH_X+2))
        {
            float xindex = ((float)(threadIdx.x -2.0f + (blockIdx.x*BLOCK_WIDTH_X))) + 0.5f ;
            float yindex = ((float)(threadIdx.y + (blockIdx.y*BLOCK_WIDTH_Y))) + 0.5f ;

            float beta;
            float buf;
            float t_akt;

            // posuneme aktualni polohu do patricneho bodu a do patra
            aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx  + 5*TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
            float zindex = ((float)((blockIdx.z*BLOCK_WIDTH_Z + 3))) + 0.5f;
            ty+=2;

            // for cyklus pres osu Z
            //#pragma unroll 4
            for (signed int i=5; i< 8; i++)
            {
                t_akt = krychlicka[i][ty][tx];
                beta = tex3D(tex_beta,xindex,yindex,zindex);

                buf =  ((krychlicka[i-2][ty][tx] - 16*krychlicka[i-1][ty][tx] + 30*t_akt - 16*krychlicka[i+1][ty][tx] + krychlicka[i+2][ty][tx]) * DELTA_Z);
                buf += ((krychlicka[i][ty-2][tx] - 16*krychlicka[i][ty-1][tx] + 30*t_akt - 16*krychlicka[i][ty+1][tx] + krychlicka[i][ty+2][tx]) * DELTA_Y);
                buf += ((krychlicka[i][ty][tx-2] - 16*krychlicka[i][ty][tx-1] + 30*t_akt - 16*krychlicka[i][ty][tx+1] + krychlicka[i][ty][tx+2]) * DELTA_X);

                outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

                // posun v ose Z o 1 nahoru
                aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
                zindex += 1.0f;
            }
        }
    }
    else
    {
        if ((tx>1) && (tx<BLOCK_WIDTH_X+2))
        {
            float xindex = ((float)(threadIdx.x -2.0f + (blockIdx.x*BLOCK_WIDTH_X)))  ;
            float yindex = ((float)(threadIdx.y + (blockIdx.y*BLOCK_WIDTH_Y)))  ;

            float beta;
            float buf;
            float t_akt;

            // posuneme aktualni polohu do patricneho bodu a do patra
            aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx  + 8*TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
            float zindex = ((float)((blockIdx.z*BLOCK_WIDTH_Z+6))) + 0.5f;
            ty+=2;

            // for cyklus pres osu Z
            //#pragma unroll 4
            for (signed int i=8; i< 10; i++)
            {
                t_akt = krychlicka[i][ty][tx];
                beta = tex3D(tex_beta,xindex,yindex,zindex);

                buf =  ((krychlicka[i-2][ty][tx] - 16*krychlicka[i-1][ty][tx] + 30*t_akt - 16*krychlicka[i+1][ty][tx] + krychlicka[i+2][ty][tx]) * DELTA_Z);
                buf += ((krychlicka[i][ty-2][tx] - 16*krychlicka[i][ty-1][tx] + 30*t_akt - 16*krychlicka[i][ty+1][tx] + krychlicka[i][ty+2][tx]) * DELTA_Y);
                buf += ((krychlicka[i][ty][tx-2] - 16*krychlicka[i][ty][tx-1] + 30*t_akt - 16*krychlicka[i][ty][tx+1] + krychlicka[i][ty][tx+2]) * DELTA_X);

                outputD[aktualni_pozice] = t_akt + DELTA_T*beta*buf;

                // posun v ose Z o 1 nahoru
                aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
                zindex += 1.0f;
            }
        }

    }

}

//=================================================
//                   KERNEL V5
//=================================================

// nepouziva se sdilena pamet, primo se pocita
// vstupni matice 260x260x260

#undef TOTAL_WIDTH_X
#define TOTAL_WIDTH_X 260
#undef TOTAL_WIDTH_Y
#define TOTAL_WIDTH_Y 260
#undef BLOCK_WIDTH_X
#define BLOCK_WIDTH_X 32
#undef BLOCK_WIDTH_Y
#define BLOCK_WIDTH_Y 8
#undef BLOCK_WIDTH_Z
#define BLOCK_WIDTH_Z 8

void runKernel_heat_V5(float *inputD, float *outputD)
{   
    dim3 dimGrid(8,32,32);
    dim3 dimBlock(32,8,1);

    // preferujeme L1 cache na ukor shared memory
    cudaFuncSetCacheConfig(kernel_heat_V5,cudaFuncCachePreferL1);

    kernel_heat_V5 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_heat_V5(float *inputD, float *outputD)
{

    // souradnice ve vstupni matici, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = (((blockIdx.z*BLOCK_WIDTH_Z)+2)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            (((blockIdx.y*BLOCK_WIDTH_Y)+2)*TOTAL_WIDTH_X) + // osa y
            ((blockIdx.x*BLOCK_WIDTH_X)+2);    // osa x

    // nacteni samotne krychlicky + dolni(i==0) a horni(i==BLOCK_WIDTH_Z+2) podstavy
    // okraje pro X==0 a X==Max jsou reseny ze se zde nacita 32 bodu, ale prumer pocitame pro 30!

    // promenne pro okoli bodu, x_p1 = pozice x plus 1 od bodu
    float beta, t_akt, x_p1,x_p2,x_m1,x_m2, y_p1,y_p2,y_m1,y_m2, z_p1,z_p2,z_m1,z_m2;

    // souradnice do textury
    float xindex = ((float)(threadIdx.x + (blockIdx.x*BLOCK_WIDTH_X))) ;
    float yindex = ((float)(threadIdx.y + (blockIdx.y*BLOCK_WIDTH_Y))) ;
    float zindex = ((float)((blockIdx.z*BLOCK_WIDTH_Z))) ;

    int aktualni_pozice = pozice_blok + (threadIdx.y*TOTAL_WIDTH_X) + threadIdx.x;

    // for cyklus pres vsechny dlazdice bloku
    for (signed int k=0; k< (BLOCK_WIDTH_Z); k++)
    {

            // nacteni z texture s matici beta
            beta = tex3D(tex_beta,xindex,yindex,zindex);

            // serazeno podle pravdepodobnosti ze se nachazi v L1 cache
            t_akt =   inputD[aktualni_pozice];

            // T0[x,y,z-2]
            z_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
            z_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X*TOTAL_WIDTH_Y];


            y_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X];
            y_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X];


            x_m2 =    inputD[aktualni_pozice - 2];
            x_m1 =    inputD[aktualni_pozice - 1];


            x_p1 =  inputD[aktualni_pozice + 1];
            x_p2 =  inputD[aktualni_pozice + 2];

            y_p1 =  inputD[aktualni_pozice + TOTAL_WIDTH_X];
            y_p2 =  inputD[aktualni_pozice + 2*TOTAL_WIDTH_X];

            z_p1 =  inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
            z_p2 =  inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y];

            // samotny vypocet rovnice
            outputD[aktualni_pozice] =
                    (t_akt + DELTA_T*beta*(
                            ((z_m2 -16*z_m1 + 30*t_akt - 16*z_p1 + z_p2) * DELTA_Z) +
                            ((y_m2 -16*y_m1 + 30*t_akt - 16*y_p1 + y_p2) * DELTA_Y) +
                            ((x_m2 -16*x_m1 + 30*t_akt - 16*x_p1 + x_p2) * DELTA_X)
                          )
                    );

            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
            zindex += 1.0f;
    }

}

//=================================================
//                   KERNEL V5_1
//=================================================

// nepouziva se sdilena pamet, primo se pocita
// vstupni matice 256x256x256, okraje zadne
// blok velikosti 8x8x32 aby se 1 cteni z pameti v ose x co nejvice vyuzilo
// block je tak maly v ose y, abychom mohli vyuzivat body nactene do L1 cache i pro dalsi patra

// vstupni matice nema prilozene okraje, dodavaji se automaticky
// aby se vedelo kdy se maji nacist hranicni konstanty, je potreba delat kontroly, kterych je ovsem mnoho => jejich redukce
// je vypracovano celkem 27 = 3^3 moznosti (3dimenze, 3moznosti) 3 moznosti: 1. okraj=> dat konstantu, bloky mezi - konstanta netreba, 2.okraj=> dat konstantu
// je jich tolik proto, ze v danem smeru a dimenzi se kontroluje zda je thread na okraji matice jen v blocich,
// ktere realne na kraji v dane dimenzi a smeru jsou

#undef TOTAL_WIDTH_X
#define TOTAL_WIDTH_X 256
#undef TOTAL_WIDTH_Y
#define TOTAL_WIDTH_Y 256
#undef BLOCK_WIDTH_X
#define BLOCK_WIDTH_X 32
#undef BLOCK_WIDTH_Y
#define BLOCK_WIDTH_Y 8
#undef BLOCK_WIDTH_Z
#define BLOCK_WIDTH_Z 8

#define BLOCK_COUNT_X 8
#define BLOCK_COUNT_Y 32
#define BLOCK_COUNT_Z 32

// ruzne nacitani bodu v ose Z podle pozice bloku v gridu
// podminky na hranicni body jsou jen v blocih, ktere realne na hranici lezi

#define Z_NORMAL \
z_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p2 =    inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \

#define Z_BZ0_K0 \
z_m1 =    TEPLOTA_TELA; \
z_m2 =    TEPLOTA_TELA; \
z_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p2 =    inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \

#define Z_BZ0_K1 \
z_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_m2 =    TEPLOTA_TELA; \
z_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p2 =    inputD[aktualni_pozice + 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \

#define Z_BZmax_Kmax \
z_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p1 =    TEPLOTA_TELA; \
z_p2 =    TEPLOTA_TELA; \

#define Z_BZmax_KmaxMin1 \
z_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; \
z_p2 =    TEPLOTA_TELA; \

// ruzne nacitani bodu v ose Y podle pozice bloku v gridu

#define Y_NORMAL \
y_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X]; \
y_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X]; \
y_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X]; \
y_p2 =    inputD[aktualni_pozice + 2*TOTAL_WIDTH_X]; \

#define Y__BY0_TY0 \
y_m1 =    TEPLOTA_TELA; \
y_m2 =    TEPLOTA_TELA; \
y_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X]; \
y_p2 =    inputD[aktualni_pozice + 2*TOTAL_WIDTH_X]; \

#define Y__BY0_TY1 \
y_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X]; \
y_m2 =    TEPLOTA_TELA; \
y_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X]; \
y_p2 =    inputD[aktualni_pozice + 2*TOTAL_WIDTH_X]; \

#define Y__BYmax_TYmaxMin1 \
y_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X]; \
y_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X]; \
y_p1 =    inputD[aktualni_pozice + TOTAL_WIDTH_X]; \
y_p2 =    TEPLOTA_TELA; \

#define Y__BYmax_TYmaxX \
y_m1 =    inputD[aktualni_pozice - TOTAL_WIDTH_X]; \
y_m2 =    inputD[aktualni_pozice - 2*TOTAL_WIDTH_X]; \
y_p1 =    TEPLOTA_TELA; \
y_p2 =    TEPLOTA_TELA; \

#define Y_BY0 \
    if (ty>1) \
        { Y_NORMAL } \
    else \
    { \
        if (ty==0) \
            { Y__BY0_TY0 } \
        else /* ty==1 */ \
            { Y__BY0_TY1 } \
    } \

#define Y_BYMAX \
    if (ty<(BLOCK_WIDTH_Y-2)) \
        { Y_NORMAL } \
    else \
    { \
        if (ty==(BLOCK_WIDTH_Y-1)) \
            { Y__BYmax_TYmaxX } \
        else /* ty==BLOCK_WIDTH_Y-2 */ \
            { Y__BYmax_TYmaxMin1 } \
    } \

// ruzne nacitani bodu v ose X podle pozice bloku v gridu

#define X_NORMAL \
x_m1 =    inputD[aktualni_pozice - 1]; \
x_m2 =    inputD[aktualni_pozice - 2]; \
x_p1 =    inputD[aktualni_pozice + 1]; \
x_p2 =    inputD[aktualni_pozice + 2]; \

#define X__BX0_TX0 \
x_m1 =    TEPLOTA_TELA; \
x_m2 =    TEPLOTA_TELA; \
x_p1 =    inputD[aktualni_pozice + 1]; \
x_p2 =    inputD[aktualni_pozice + 2]; \

#define X__BX0_TX1 \
x_m1 =    inputD[aktualni_pozice - 1]; \
x_m2 =    TEPLOTA_TELA; \
x_p1 =    inputD[aktualni_pozice + 1]; \
x_p2 =    inputD[aktualni_pozice + 2]; \

#define X__BXmax_TXmaxMin1 \
x_m1 =    inputD[aktualni_pozice - 1]; \
x_m2 =    inputD[aktualni_pozice - 2]; \
x_p1 =    inputD[aktualni_pozice + 1]; \
x_p2 =    TEPLOTA_TELA; \

#define X__BXmax_TXmax \
x_m1 =    inputD[aktualni_pozice - 1]; \
x_m2 =    inputD[aktualni_pozice - 2]; \
x_p1 =    TEPLOTA_TELA; \
x_p2 =    TEPLOTA_TELA; \

#define X_BX0 \
    if (tx>1) \
        { X_NORMAL } \
    else \
    { \
        if (tx==0) \
            { X__BX0_TX0 } \
        else /* tx==1 */ \
            { X__BX0_TX1 } \
    } \

#define X_BXMAX \
    if (tx<(BLOCK_WIDTH_X-2)) \
        { X_NORMAL } \
    else \
    { \
        if (tx==(BLOCK_WIDTH_X-1)) \
            { X__BXmax_TXmax } \
        else /* tx==BLOCK_WIDTH_X-2 */ \
            { X__BXmax_TXmaxMin1 } \
    } \


//beta = tex1Dfetch(tex_beta2,aktualni_pozice);

// makro pro samotny vypocet 1 bodu, parametry dodaji do prislusnych os polohy pro nacitani bodu nebo konstantu
#define VYPOCET(X,Y,Z) \
    beta = tex3D(tex_beta,xindex,yindex,zindex); \
    t_akt =   inputD[aktualni_pozice]; \
    Z \
    Y \
    X \
    outputD[aktualni_pozice] = \
            (t_akt + DELTA_T*beta*( \
                 ((z_m2 -16.0f*z_m1 + 30.0f*t_akt -16.0f*z_p1 + z_p2) * DELTA_Z) + \
                 ((y_m2 -16.0f*y_m1 + 30.0f*t_akt -16.0f*y_p1 + y_p2) * DELTA_Y) + \
                 ((x_m2 -16.0f*x_m1 + 30.0f*t_akt -16.0f*x_p1 + x_p2) * DELTA_X) \
                 ) \
             ); \
    aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y; \
    zindex += 1.0f; \

void runKernel_heat_V5_1(float *inputD, float *outputD)
{
    dim3 dimGrid(BLOCK_COUNT_X,BLOCK_COUNT_Y,BLOCK_COUNT_Z);
    dim3 dimBlock(32,8,1);

    // preferujeme L1 cache na ukor shared memory
    cudaFuncSetCacheConfig(kernel_heat_V5_1,cudaFuncCachePreferL1);

    kernel_heat_V5_1 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_heat_V5_1(float *inputD, float *outputD)
{
    int bx = blockIdx.x;
    int by = blockIdx.y;
    int bz = blockIdx.z;

    int tx = threadIdx.x;
    int ty = threadIdx.y;

    // souradnice ve vstupni matici, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = (((bz*BLOCK_WIDTH_Z))*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            (((by*BLOCK_WIDTH_Y))*TOTAL_WIDTH_X) + // osa y
            ((bx*BLOCK_WIDTH_X));    // osa x

    // promenne pro bod a okoli bodu, x_p1 = pozice bodu +1 od bodu v ose x
    float beta, t_akt, x_p1,x_p2,x_m1,x_m2, y_p1,y_p2,y_m1,y_m2, z_p1,z_p2,z_m1,z_m2;

    // souradnice do textury s parametrem beta
    float xindex = ((float)(tx + (bx*BLOCK_WIDTH_X)));
    float yindex = ((float)(ty + (by*BLOCK_WIDTH_Y)));
    float zindex = ((float)((bz*BLOCK_WIDTH_Z)));


    // pozice threadu v bloku
    int aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) + tx;    


    if (bz>0)
    {
        // BZ=NORMAL
        // ================================================================
        if (bz<(BLOCK_COUNT_Z-1))
        {
            if (by>0)
            {
                // BY=NORMAL
                //-----------------------------------------------------------
                if (by<(BLOCK_COUNT_Y-1))
                {
                    if (bx>0)
                    {
                        // BX=NORMAL
                        if (bx<(BLOCK_COUNT_X-1))
                        {
                            #pragma unroll 8
                            for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_NORMAL,Y_NORMAL,Z_NORMAL) }
                        }
                        // BX=MAX
                        else
                        {
                            #pragma unroll 8
                            for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BXMAX,Y_NORMAL,Z_NORMAL) }
                        }
                    }
                    // BX=0
                    else
                    {
                        #pragma unroll 8
                        for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BX0,Y_NORMAL,Z_NORMAL) }
                    }
                }
                // BY=MAX
                //---------------------------------------------------------------
                else
                {
                    if (bx>0)
                    {
                        // BX=NORMAL
                        if (bx<(BLOCK_COUNT_X-1))
                        {
                            #pragma unroll 8
                            for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_NORMAL,Y_BYMAX,Z_NORMAL) }
                        }
                        // BX=MAX
                        else
                        {
                            #pragma unroll 8
                            for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BXMAX,Y_BYMAX,Z_NORMAL) }
                        }
                    }
                    // BX=0
                    else
                    {
                        #pragma unroll 8
                        for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BX0,Y_BYMAX,Z_NORMAL) }
                    }

                }
            }
            // BY=0
            //-----------------------------------------------------------------
            else
            {
                if (bx>0)
                {
                    // BX=NORMAL
                    if (bx<(BLOCK_COUNT_X-1))
                    {
                        #pragma unroll 8
                        for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_NORMAL,Y_BY0,Z_NORMAL) }
                    }
                    // BX=MAX
                    else
                    {
                        #pragma unroll 8
                        for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BXMAX,Y_BY0,Z_NORMAL) }
                    }
                }
                // BX=0
                else
                {
                    #pragma unroll 8
                    for (signed int k=0; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BX0,Y_BY0,Z_NORMAL) }
                }
            }

        }


        // BZ=MAX
        // ================================================================
        else
        {
            if (by>0)
            {
                // BY=NORMAL
                //-----------------------------------------------------------
                if (by<(BLOCK_COUNT_Y-1))
                {
                    if (bx>0)
                    {
                        // BX=NORMAL
                        if (bx<(BLOCK_COUNT_X-1))
                        {
                            for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_NORMAL,Y_NORMAL,Z_NORMAL) }
                            VYPOCET(X_NORMAL,Y_NORMAL,Z_BZmax_KmaxMin1)
                            VYPOCET(X_NORMAL,Y_NORMAL,Z_BZmax_Kmax)
                        }
                        // BX=MAX
                        else
                        {
                            for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_BXMAX,Y_NORMAL,Z_NORMAL) }
                            VYPOCET(X_BXMAX,Y_NORMAL,Z_BZmax_KmaxMin1)
                            VYPOCET(X_BXMAX,Y_NORMAL,Z_BZmax_Kmax)
                        }
                    }
                    // BX=0
                    else
                    {
                        for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_BX0,Y_NORMAL,Z_NORMAL) }
                        VYPOCET(X_BX0,Y_NORMAL,Z_BZmax_KmaxMin1)
                        VYPOCET(X_BX0,Y_NORMAL,Z_BZmax_Kmax)
                    }
                }
                // BY=MAX
                //---------------------------------------------------------------
                else
                {
                    if (bx>0)
                    {
                        // BX=NORMAL
                        if (bx<(BLOCK_COUNT_X-1))
                        {
                            for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_NORMAL,Y_BYMAX,Z_NORMAL) }
                            VYPOCET(X_NORMAL,Y_BYMAX,Z_BZmax_KmaxMin1)
                            VYPOCET(X_NORMAL,Y_BYMAX,Z_BZmax_Kmax)
                        }
                        // BX=MAX
                        else
                        {
                            for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_BXMAX,Y_BYMAX,Z_NORMAL) }
                            VYPOCET(X_BXMAX,Y_BYMAX,Z_BZmax_KmaxMin1)
                            VYPOCET(X_BXMAX,Y_BYMAX,Z_BZmax_Kmax)
                        }
                    }
                    // BX=0
                    else
                    {
                        for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_BX0,Y_BYMAX,Z_NORMAL) }
                        VYPOCET(X_BX0,Y_BYMAX,Z_BZmax_KmaxMin1)
                        VYPOCET(X_BX0,Y_BYMAX,Z_BZmax_Kmax)
                    }

                }
            }
            // BY=0
            //-----------------------------------------------------------------
            else
            {
                if (bx>0)
                {
                    // BX=NORMAL
                    if (bx<(BLOCK_COUNT_X-1))
                    {
                        for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_NORMAL,Y_BY0,Z_NORMAL) }
                        VYPOCET(X_NORMAL,Y_BY0,Z_BZmax_KmaxMin1)
                        VYPOCET(X_NORMAL,Y_BY0,Z_BZmax_Kmax)
                    }
                    // BX=MAX
                    else
                    {
                        for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_BXMAX,Y_BY0,Z_NORMAL) }
                        VYPOCET(X_BXMAX,Y_BY0,Z_BZmax_KmaxMin1)
                        VYPOCET(X_BXMAX,Y_BY0,Z_BZmax_Kmax)
                    }
                }
                // BX=0
                else
                {
                    for (signed int k=0; k< (BLOCK_WIDTH_Z-2); k++) { VYPOCET(X_BX0,Y_BY0,Z_NORMAL) }
                    VYPOCET(X_BX0,Y_BY0,Z_BZmax_KmaxMin1)
                    VYPOCET(X_BX0,Y_BY0,Z_BZmax_Kmax)
                }
            }

        }
    }
    // BZ=0
    // ================================================================
    else
    {
        if (by>0)
        {
            // BY=NORMAL
            //-----------------------------------------------------------
            if (by<(BLOCK_COUNT_Y-1))
            {
                if (bx>0)
                {
                    // BX=NORMAL
                    if (bx<(BLOCK_COUNT_X-1))
                    {
                        VYPOCET(X_NORMAL,Y_NORMAL,Z_BZ0_K0)
                        VYPOCET(X_NORMAL,Y_NORMAL,Z_BZ0_K1)
                        for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_NORMAL,Y_NORMAL,Z_NORMAL) }
                    }
                    // BX=MAX
                    else
                    {
                        VYPOCET(X_BXMAX,Y_NORMAL,Z_BZ0_K0)
                        VYPOCET(X_BXMAX,Y_NORMAL,Z_BZ0_K1)
                        for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BXMAX,Y_NORMAL,Z_NORMAL) }
                    }
                }
                // BX=0
                else
                {
                    VYPOCET(X_BX0,Y_NORMAL,Z_BZ0_K0)
                    VYPOCET(X_BX0,Y_NORMAL,Z_BZ0_K1)
                    for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BX0,Y_NORMAL,Z_NORMAL) }
                }
            }
            // BY=MAX
            //---------------------------------------------------------------
            else
            {
                if (bx>0)
                {
                    // BX=NORMAL
                    if (bx<(BLOCK_COUNT_X-1))
                    {
                        VYPOCET(X_NORMAL,Y_BYMAX,Z_BZ0_K0)
                        VYPOCET(X_NORMAL,Y_BYMAX,Z_BZ0_K1)
                        for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_NORMAL,Y_BYMAX,Z_NORMAL) }
                    }
                    // BX=MAX
                    else
                    {
                        VYPOCET(X_BXMAX,Y_BYMAX,Z_BZ0_K0)
                        VYPOCET(X_BXMAX,Y_BYMAX,Z_BZ0_K1)
                        for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BXMAX,Y_BYMAX,Z_NORMAL) }
                    }
                }
                // BX=0
                else
                {
                    VYPOCET(X_BX0,Y_BYMAX,Z_BZ0_K0)
                    VYPOCET(X_BX0,Y_BYMAX,Z_BZ0_K1)
                    for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BX0,Y_BYMAX,Z_NORMAL) }
                }

            }
        }
        // BY=0
        //-----------------------------------------------------------------
        else
        {
            if (bx>0)
            {
                // BX=NORMAL
                if (bx<(BLOCK_COUNT_X-1))
                {
                    VYPOCET(X_NORMAL,Y_BY0,Z_BZ0_K0)
                    VYPOCET(X_NORMAL,Y_BY0,Z_BZ0_K1)
                    for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_NORMAL,Y_BY0,Z_NORMAL) }
                }
                // BX=MAX
                else
                {
                    VYPOCET(X_BXMAX,Y_BY0,Z_BZ0_K0)
                    VYPOCET(X_BXMAX,Y_BY0,Z_BZ0_K1)
                    for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BXMAX,Y_BY0,Z_NORMAL) }
                }
            }
            // BX=0
            else
            {
                VYPOCET(X_BX0,Y_BY0,Z_BZ0_K0)
                VYPOCET(X_BX0,Y_BY0,Z_BZ0_K1)
                for (signed int k=2; k< (BLOCK_WIDTH_Z); k++) { VYPOCET(X_BX0,Y_BY0,Z_NORMAL) }
            }
        }
    }

}


/*
 * debugovaci kod - vypise obsah krychlicky v urcite threadu a blocku
 *
if ( (blockIdx.x==0) && (blockIdx.y==0) && (blockIdx.z==0) && (tz==0) && (ty==0) && (tx==0))
{
    for (int k=0; k<10 ;k++)
    {
        for (int j=0; j<10 ;j++)
        {
            for (int i=0; i<10 ;i++)
            {
                printf("%f\t",krychlicka[k][j][i]);
            }
            printf("\n");
        }
        printf("--------\n");
    }
}
*/

