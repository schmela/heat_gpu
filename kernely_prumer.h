/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#ifndef KERNELY_PRUMER_H
#define KERNELY_PRUMER_H

// spousteci funkce pro cuda kernely, ktere externuji spousteni mimo cu soubory
void runKernel_prumer_V1(float *inputD, float *outputD);
void runKernel_prumer_V2(float *inputD, float *outputD);
void runKernel_prumer_V2_1(float *inputD, float *outputD);
void runKernel_prumer_V2_2(float *inputD, float *outputD);
void runKernel_prumer_V3(float *inputD, float *outputD);
void runKernel_prumer_V3_1(float *inputD, float *outputD);
void runKernel_prumer_V4(float *inputD, float *outputD);
void runKernel_prumer_V5(float *inputD, float *outputD);

#endif // KERNELS_PRUMER_H
