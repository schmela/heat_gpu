/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#ifndef HDF5_WRAPPER_H
#define HDF5_WRAPPER_H

#include <string>
#include "H5Cpp.h"

using namespace H5;

class hdf5_wrapper
{
public:
    hdf5_wrapper();
    ~hdf5_wrapper();

    // nacte matici z datasetu v HDF5 souboru a ulozi ji do inputMatrix
    void loadMatrixFromFile(std::string fileName, std::string datasetName);
    // zapise danou matici datasetu HDF5 souboru
    void writeMatrixToFile(std::string fileName, std::string datasetName,float *matrix);
    // vypise zadanou matice na vystup
    void printMatrix(float *data);

    float *inputMatrix;    // ulozena matice
    int dims;              // pocet dimenzi matice inputMatrix
    int dimX, dimY, dimZ;  // rozmery dimenzi matice inputMatrix
};

#endif // HDF5_WRAPPER_H
