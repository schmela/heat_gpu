/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

/*
části kódu pro načítání a zapsání byly využity z příkladů:
http://www.hdfgroup.org/ftp/HDF5/current/src/unpacked/c++/examples/readdata.cpp
http://www.hdfgroup.org/ftp/HDF5/current/src/unpacked/c++/examples/writedata.cpp

Licence: http://www.hdfgroup.org/HDF5/doc/Copyright.html


HDF5 (Hierarchical Data Format 5) Software Library and Utilities
Copyright 2006-2013 by The HDF Group.

NCSA HDF5 (Hierarchical Data Format 5) Software Library and Utilities
Copyright 1998-2006 by the Board of Trustees of the University of Illinois.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted for any purpose (including commercial purposes) provided that the following conditions are met:

   Redistributions of source code must retain the above copyright notice, this list of conditions, and the following disclaimer.
   Redistributions in binary form must reproduce the above copyright notice, this list of conditions, and the following disclaimer in the documentation and/or materials provided with the distribution.
   In addition, redistributions of modified forms of the source or binary code must carry prominent notices stating that the original code was changed and the date of the change.
   All publications or advertising materials mentioning features or use of this software are asked, but not required, to acknowledge that it was developed by The HDF Group and by the National Center for Supercomputing Applications at the University of Illinois at Urbana-Champaign and credit the contributors.
   Neither the name of The HDF Group, the name of the University, nor the name of any Contributor may be used to endorse or promote products derived from this software without specific prior written permission from The HDF Group, the University, or the Contributor, respectively.

DISCLAIMER: THIS SOFTWARE IS PROVIDED BY THE HDF GROUP AND THE CONTRIBUTORS "AS IS" WITH NO WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED. In no event shall The HDF Group or the Contributors be liable for any damages suffered by the users arising out of the use of this software, even if advised of the possibility of such damage.
*/

#include <iostream>

#include "hdf5_wrapper.h"
#include "H5Cpp.h"

using namespace H5;

hdf5_wrapper::hdf5_wrapper()
{
    inputMatrix = NULL;
}

hdf5_wrapper::~hdf5_wrapper()
{       
    if (inputMatrix != NULL) delete inputMatrix;
}

// otevre soubor a dany dataset
void hdf5_wrapper::loadMatrixFromFile(std::string fileName, std::string datasetName)
{
    H5File *file=NULL;
    DataSet dataset;
    DataSpace dataspace;

	// otevre soubor a v nem zadany dataset
    file = new H5File( fileName, H5F_ACC_RDONLY );
    dataset = file->openDataSet( datasetName );

	// zjisteni typu datasetu
    H5T_class_t typeClass = dataset.getTypeClass();

	// jestli neni typu FLOAT, vypise se varovani
    if( typeClass != H5T_FLOAT )
    {
        std::cerr << "Warning: Dataset neni typu FLOAT!" << std::endl;
    }

	// zjisteni poctu dimenzi
    dataspace = dataset.getSpace();
    dims = dataspace.getSimpleExtentNdims();

    if( dims != 3 )
    {
        std::cerr << "Warning: Dataset " <<  fileName << "/" << datasetName << " ma " << dims << " dimenzi !!" << std::endl;
    }
    else
    {
        std::cout << "Dataset "<<  fileName << "/" << datasetName << " dimenzi: " << dims << " ";
    }

    // zjisteni delek dimenzi
    hsize_t dimenze[3] = {1,1,1};
    int ndims = dataspace.getSimpleExtentDims( dimenze, NULL);


    if (dimenze[2] == 1)
    {
        // 2D
        dimZ = dimenze[2];
        dimY = dimenze[0];
        dimX = dimenze[1];
    }
    else
    {
        // 3D
        dimZ = dimenze[0];
        dimY = dimenze[1];
        dimX = dimenze[2];
    }

    std::cout << "rozmery " <<
        (unsigned long)(dimX) << "X x " <<
        (unsigned long)(dimY) << "Y x " <<
        (unsigned long)(dimZ) << "Z" << std::endl;

    // alokace pameti pro nactena data - musi byt souvisla pamet
    inputMatrix = new float[dimX*dimY*dimZ];

    // nacteni celeho datasetu do pameti
    dataset.read( inputMatrix, PredType::NATIVE_FLOAT );

    file->close();
    delete file;

    return;
}

void hdf5_wrapper::writeMatrixToFile(std::string fileName, std::string datasetName, float *matrix)
{
    try
    {
        /*
        * Turn off the auto-printing when failure occurs so that we can
        * handle the errors appropriately
        */
        Exception::dontPrint();

        /*
     * Create a new file using H5F_ACC_TRUNC access,
     * default file creation properties, and default file
     * access properties.
     */
        H5File file( fileName, H5F_ACC_TRUNC );

        /*
     * Define the size of the array and create the data space for fixed
     * size dataset.
     */
        hsize_t dimenze[3] = {dimZ,dimY,dimX};
        DataSpace dataspace( 3, dimenze );

        /*
     * Define datatype for the data in the file.
     * We will store little endian INT numbers.
     */
        FloatType datatype( PredType::NATIVE_FLOAT );
        //datatype.setOrder( H5T_ORDER );

        /*
     * Create a new dataset within the file using defined dataspace and
     * datatype and default dataset creation properties.
     */
        DataSet dataset = file.createDataSet( datasetName, datatype, dataspace );

        /*
     * Write the data to the dataset using default memory space, file
     * space, and transfer properties.
     */
        dataset.write( matrix, PredType::NATIVE_FLOAT );
    }  // end of try block

    // catch failure caused by the H5File operations
    catch( FileIException error )
    {
        error.printError();
        return;
    }

    // catch failure caused by the DataSet operations
    catch( DataSetIException error )
    {
        error.printError();
        return;
    }

    // catch failure caused by the DataSpace operations
    catch( DataSpaceIException error )
    {
        error.printError();
        return;
    }

    // catch failure caused by the DataSpace operations
    catch( DataTypeIException error )
    {
        error.printError();
        return;
    }

    return;  // successfully terminated

}

// vypise 2D i 3D matici
void hdf5_wrapper::printMatrix(float *data)
{

    for (int k = 0; k < dimZ; k++)
    {
        // u 3D matic se primo vypisuje Z souradnice
        if (dimZ != 1) std::cout << "Z = " << k << std::endl << "------------" << std::endl;

        for (int j = 0; j < dimY; j++)
        {
            for (int i = 0; i < dimX; i++)
                std::cout << data[k*dimY*dimX + j*dimX + i] << '\t';
            std::cout << std::endl;
        }

        std::cout << std::endl << std::endl;
    }

}
