/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#ifndef KERNELY_HEAT_H
#define KERNELY_HEAT_H

// spousteci funkce pro cuda kernely, ktere externuji spousteni mimo cu soubory
void runKernel_heat_V1(float *inputD, float *outputD);
void runKernel_heat_V2_2(float *inputD, float *outputD);
void runKernel_heat_V3(float *inputD, float *outputD);
void runKernel_heat_V4_1(float *inputD, float *outputD);
void runKernel_heat_V5(float *inputD, float *outputD);
void runKernel_heat_V5_1(float *inputD, float *outputD);

// pripravi texturu s matici Beta pro pouziti
cudaArray *prepareTexture(float *beta, int dimx, int dimy, int dimz);
// uvolni texturu alokovanou funkci prepareTexture()
void freeTexture(cudaArray *cudaarray);

#endif // KERNELY_HEAT_H
