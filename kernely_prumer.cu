/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#include <stdio.h>
#include <cuda_runtime.h>

#include "kernely_prumer.h"
#include "defines.h"

// definice kernulu pro spousteci funkce runKernel_prumer_Vxxx
__global__ void kernel_prumer_V1(float *inputD, float *outputD);
__global__ void kernel_prumer_V2(float *inputD, float *outputD);
__global__ void kernel_prumer_V2_1(float *inputD, float *outputD);
__global__ void kernel_prumer_V2_2(float *inputD, float *outputD);
__global__ void kernel_prumer_V3(float *inputD, float *outputD);
__global__ void kernel_prumer_V3_1(float *inputD, float *outputD);
__global__ void kernel_prumer_V4(float *inputD, float *outputD);
__global__ void kernel_prumer_V5(float *inputD, float *outputD);


//=================================================
//                   KERNEL V1 - naivni
//=================================================

// zadne vyuziti sdilene pameti...
// matice je rozdelena do krychlicek 8x8x8, kde
// vstupni matice 258x258x258 [Z,Y,X]

#define TOTAL_WIDTH 258
#define BLOCK_WIDTH 8

void runKernel_prumer_V1(float *inputD, float *outputD)
{    
    dim3 dimGrid(32,32,32);
    dim3 dimBlock(8,8,8);

    kernel_prumer_V1 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_prumer_V1(float *inputD, float *outputD)
{
    int tz = threadIdx.z;
    int ty = threadIdx.y;
    int tx = threadIdx.x;

    int bz = blockIdx.z;
    int by = blockIdx.y;
    int bx = blockIdx.x;

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    // (vsude +1 protoze je okraj o velikosti 1 ve vsech dimenzich)
    int pozice_blok = ((bz*BLOCK_WIDTH +1)*TOTAL_WIDTH*TOTAL_WIDTH) + // osa z
            ((by*BLOCK_WIDTH+1)*TOTAL_WIDTH) + // osa y
            (bx*BLOCK_WIDTH+1);    // osa x


    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (tz*TOTAL_WIDTH*TOTAL_WIDTH) + (ty*TOTAL_WIDTH) +tx;

    outputD[aktualni_pozice] = (
                inputD[aktualni_pozice + TOTAL_WIDTH] +
                inputD[aktualni_pozice + TOTAL_WIDTH*TOTAL_WIDTH] +
                inputD[aktualni_pozice + 1] +
                inputD[aktualni_pozice - TOTAL_WIDTH*TOTAL_WIDTH] +
                inputD[aktualni_pozice - TOTAL_WIDTH] +
                inputD[aktualni_pozice - 1]
                ) * 0.1666666666f; // jakoze deleno 6

}



//=================================================
//                   KERNEL V2
//=================================================

// jiz se vyuziva sdilena pamet
// blok 8x8x8 threadu
// nacteme 8x8x8 + okoli = 10x10x10 do krychlicky sdilene pameti
// kazdy thread nacita svoje + okoli se udelala pomoci podminek
// udela prumer pro samotnych 8x8x8 bodu a zapise se zpet
// vstupni matice 258x258x258 [Z,Y,X]

// spusteni kernelu
void runKernel_prumer_V2(float *inputD, float *outputD)
{
    dim3 dimGrid(32,32,32);
    dim3 dimBlock(8,8,8);

    kernel_prumer_V2 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_prumer_V2(float *inputD, float *outputD)
{

    int tz = threadIdx.z;
    int ty = threadIdx.y;
    int tx = threadIdx.x;

    int tz_plus1 = tz+1;
    int ty_plus1 = ty+1;
    int tx_plus1 = tx+1;

    __shared__ float krychlicka[10][10][10];

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    // (vsude +1 protoze je okraj o velikosti 1 ve vsech dimenzich)
    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH+1)*TOTAL_WIDTH*TOTAL_WIDTH) + // osa z
            ((blockIdx.y*BLOCK_WIDTH+1)*TOTAL_WIDTH) + // osa y
            (blockIdx.x*BLOCK_WIDTH+1);    // osa x

    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (tz*TOTAL_WIDTH*TOTAL_WIDTH) + (ty*TOTAL_WIDTH) +tx;

    // kazdy thread nacita svoje
    krychlicka[tz_plus1][ty_plus1][tx_plus1] = inputD[aktualni_pozice];

    // nacteni hranic
    if (tx==0) krychlicka[tz_plus1][ty_plus1][tx] = inputD[aktualni_pozice-1];
    if (tx==(BLOCK_WIDTH-1)) krychlicka[tz_plus1][ty_plus1][tx+2] = inputD[aktualni_pozice+1];

    if (ty==0) krychlicka[tz_plus1][ty][tx_plus1] = inputD[aktualni_pozice-TOTAL_WIDTH];
    if (ty==(BLOCK_WIDTH-1)) krychlicka[tz_plus1][ty+2][tx_plus1] = inputD[aktualni_pozice+TOTAL_WIDTH];

    if (tz==0) krychlicka[tz][ty_plus1][tx_plus1] = inputD[aktualni_pozice-TOTAL_WIDTH*TOTAL_WIDTH];
    if (tz==(BLOCK_WIDTH-1)) krychlicka[tz+2][ty_plus1][tx_plus1] = inputD[aktualni_pozice+TOTAL_WIDTH*TOTAL_WIDTH];

    __syncthreads();

    // vypocet + zapsani zpet
    outputD[aktualni_pozice] = (
                krychlicka[tz_plus1][ty_plus1][tx+2] +
                krychlicka[tz_plus1][ty_plus1][tx] +

                krychlicka[tz_plus1][ty+2][tx_plus1] +
                krychlicka[tz_plus1][ty][tx_plus1] +

                krychlicka[tz+2][ty_plus1][tx_plus1] +
                krychlicka[tz][ty_plus1][tx_plus1]
                ) * 0.1666666666f; // jakoze deleno 6

}


//=================================================
//                   KERNEL V2_1
//=================================================

// vyuziva se sdilena pamet
// blok 8x8x8 threadu
// nacteme 8x8x8 + okoli = 10x10x10 do krychlicky sdilene pameti
// kazdy thread nacita svoje + okoli se udelala pomoci podminek
// udela prumer pro samotnych 8x8x8 bodu a zapise se zpet
// --
// optimalizace nacitani, kazda deska (stejna osa z) nacita jednu stenu krychlicky
// => uz nestoji vetsina threadu ve warpu, nacitaji bud vsechny thready ve warpu nebo zadny
// vstupni matice 258x258x272, v ose x je z kazde strany 7 sloupcu navic kvuli zarovnani pameti
// kompilovat s vypnutou L1 cache

void runKernel_prumer_V2_1(float *inputD, float *outputD)
{
    dim3 dimGrid(32,32,32);
    dim3 dimBlock(8,8,8);

    kernel_prumer_V2_1 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

#undef TOTAL_WIDTH
#define TOTAL_WIDTH_X 272
#define TOTAL_WIDTH_Y 258

__global__ void kernel_prumer_V2_1(float *inputD, float *outputD)
{

    int tz = threadIdx.z;
    int ty = threadIdx.y;
    int tx = threadIdx.x;

    int tz_plus1 = tz+1;
    int ty_plus1 = ty+1;
    int tx_plus1 = tx+1;

    __shared__ float krychlicka[10][10][24];

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    // (vsude +1 protoze je okraj o velikosti 1 ve vsech dimenzich)
    // + 7 kvuli zarovnani pameti, v ose x je z kazde strany 7 sloupcu navic
    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH+1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            ((blockIdx.y*BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + // osa y
            (blockIdx.x*BLOCK_WIDTH+1) + 7;    // osa x, 7 kvuli zarovnani pameti

    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (tz*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (ty*TOTAL_WIDTH_X) +tx;

    // kazdy thread nacita svoje
    krychlicka[tz_plus1][ty_plus1][tx_plus1] = inputD[aktualni_pozice];

    // nacteni hranic
    if (tz==0) krychlicka[0][ty_plus1][tx_plus1] = inputD[aktualni_pozice-TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; // dolni postava, z==0
    if (tz==1) krychlicka[BLOCK_WIDTH+1][ty_plus1][tx_plus1] = inputD[aktualni_pozice+(BLOCK_WIDTH-1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; // horni podstava z==max

    if (tz==2) krychlicka[ty_plus1][tx_plus1][0] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) -1]; // x==0
    if (tz==3) krychlicka[ty_plus1][tx_plus1][BLOCK_WIDTH+1] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) + BLOCK_WIDTH]; //x==max

    if (tz==4) krychlicka[ty_plus1][0][tx_plus1] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X ]; // y==0
    if (tz==5) krychlicka[ty_plus1][BLOCK_WIDTH+1][tx_plus1] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx - TOTAL_WIDTH_X]; // y==max

    __syncthreads();

    // vypocet + zapsani zpet
    outputD[aktualni_pozice] = (
                krychlicka[tz_plus1][ty_plus1][tx+2] +
                krychlicka[tz_plus1][ty_plus1][tx] +

                krychlicka[tz_plus1][ty+2][tx_plus1] +
                krychlicka[tz_plus1][ty][tx_plus1] +

                krychlicka[tz+2][ty_plus1][tx_plus1] +
                krychlicka[tz][ty_plus1][tx_plus1]
                ) * 0.1666666666f; // jakoze deleno 6

}

//=================================================
//                   KERNEL V2_2
//=================================================

// vyuziva se sdilena pamet
// blok 8x8x8 threadu
// nacteme 8x8x8 + okoli = 10x10x10 do krychlicky sdilene pameti
// kazdy thread nacita svoje + okoli se udelala pomoci podminek
// udela prumer pro samotnych 8x8x8 bodu a zapise se zpet
//--
// vstupni matice 258x258x272, v ose x je z kazde strany 7 sloupcu navic kvuli zarovnani pameti
//
// krychlicka sdilene pameti ma ted 10x10x40 a pocita 4 male 8x8x8 krychlicky
// teoreticky by stacilo 10x10x34, ale je pridano dalsich 6, aby se vyhnulo shared memory banks conflicts
//
// optimalizace nacitani, kazda deska (stejna osa z) nacita jednu stenu krychlicky
// => uz nestoji vetsina threadu ve warpu, nacitaji bud vsechny thready ve warpu nebo zadny
//
// pridano volatile u definice shared memory (cca -0.05ms)

void runKernel_prumer_V2_2(float *inputD, float *outputD)
{
    dim3 dimGrid(8,32,32);
    dim3 dimBlock(8,8,8);

    kernel_prumer_V2_2 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

#undef TOTAL_WIDTH
#define TOTAL_WIDTH_X 272
#define TOTAL_WIDTH_Y 258

__global__ void kernel_prumer_V2_2(float *inputD, float *outputD)
{

    int tz = threadIdx.z;
    int ty = threadIdx.y;
    int tx = threadIdx.x;

    int tz_plus1 = tz+1;
    int ty_plus1 = ty+1;
    int tx_plus1 = tx+1;

    // stacilo by [10][10][34] ale je tam 40, aby se predeslo shared memory banks conflict,
    volatile __shared__ float krychlicka[10][10][40];

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    // (vsude +1 protoze je okraj o velikosti 1 ve vsech dimenzich)
    // + 7 kvuli zarovnani pameti, v ose x je z kazde strany 7 sloupcu navic
    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH+1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            ((blockIdx.y*BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + // osa y
            (blockIdx.x*(BLOCK_WIDTH*4)+1) + 7;    // osa x, 7 kvuli zarovnani pameti

    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (tz*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (ty*TOTAL_WIDTH_X) +tx;


    /* pohled zhora v ose z na 1 blok
     *        _E_E_E_E_
     *      A |1|2|3|4| B
     *         F F F F
     */

    // kazdy thread nacita svou aktualni pozici
    krychlicka[tz_plus1][ty_plus1][tx_plus1] = inputD[aktualni_pozice];  // samotna krychlicka 1
    krychlicka[tz_plus1][ty_plus1][tx_plus1+BLOCK_WIDTH] = inputD[aktualni_pozice+BLOCK_WIDTH]; // samotna krychlicka 2
    krychlicka[tz_plus1][ty_plus1][tx_plus1+2*BLOCK_WIDTH] = inputD[aktualni_pozice+2*BLOCK_WIDTH]; // samotna krychlicka 3
    krychlicka[tz_plus1][ty_plus1][tx_plus1+3*BLOCK_WIDTH] = inputD[aktualni_pozice+3*BLOCK_WIDTH]; // samotna krychlicka 4

    // nacteni hranic

    // okraj A
    if (tz==0) krychlicka[ty_plus1][tx_plus1][0] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) -1]; // x==0

    // okraj B
    if (tz==1) krychlicka[ty_plus1][tx_plus1][BLOCK_WIDTH+1 + 3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (tx*TOTAL_WIDTH_X) + BLOCK_WIDTH + 3*BLOCK_WIDTH]; //x==max // okraj C


    if (tz==4) // okraj E
    {
        krychlicka[ty_plus1][0][tx_plus1] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X ]; // y==0
        krychlicka[ty_plus1][0][tx_plus1+BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X + BLOCK_WIDTH]; // y==0
        krychlicka[ty_plus1][0][tx_plus1+2*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X + 2*BLOCK_WIDTH]; // y==0
        krychlicka[ty_plus1][0][tx_plus1+3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + tx - TOTAL_WIDTH_X + 3*BLOCK_WIDTH]; // y==0
    }
    if (tz==5) // okraj F
    {
        krychlicka[ty_plus1][BLOCK_WIDTH+1][tx_plus1] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx - TOTAL_WIDTH_X]; // y==max
        krychlicka[ty_plus1][BLOCK_WIDTH+1][tx_plus1+BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx - TOTAL_WIDTH_X + BLOCK_WIDTH]; // y==max
        krychlicka[ty_plus1][BLOCK_WIDTH+1][tx_plus1+2*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx - TOTAL_WIDTH_X + 2*BLOCK_WIDTH]; // y==max
        krychlicka[ty_plus1][BLOCK_WIDTH+1][tx_plus1+3*BLOCK_WIDTH] = inputD[pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + ((BLOCK_WIDTH+1)*TOTAL_WIDTH_X) + tx - TOTAL_WIDTH_X + 3*BLOCK_WIDTH]; // y==max
    }

    if (tz==6) // dolni podstava
    {
        krychlicka[0][ty_plus1][tx_plus1] = inputD[aktualni_pozice-TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; // dolni postava, z==0
        krychlicka[0][ty_plus1][tx_plus1+BLOCK_WIDTH] = inputD[aktualni_pozice-TOTAL_WIDTH_X*TOTAL_WIDTH_Y+BLOCK_WIDTH]; // dolni postava, z==0
        krychlicka[0][ty_plus1][tx_plus1+2*BLOCK_WIDTH] = inputD[aktualni_pozice-TOTAL_WIDTH_X*TOTAL_WIDTH_Y+2*BLOCK_WIDTH]; // dolni postava, z==0
        krychlicka[0][ty_plus1][tx_plus1+3*BLOCK_WIDTH] = inputD[aktualni_pozice-TOTAL_WIDTH_X*TOTAL_WIDTH_Y+3*BLOCK_WIDTH]; // dolni postava, z==0
    }

    if (tz==7) // horni podstava
    {
        krychlicka[BLOCK_WIDTH+1][ty_plus1][tx_plus1] = inputD[aktualni_pozice+(BLOCK_WIDTH-1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y]; // horni podstava z==max
        krychlicka[BLOCK_WIDTH+1][ty_plus1][tx_plus1+BLOCK_WIDTH] = inputD[aktualni_pozice+(BLOCK_WIDTH-1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + BLOCK_WIDTH]; // horni podstava z==max
        krychlicka[BLOCK_WIDTH+1][ty_plus1][tx_plus1+2*BLOCK_WIDTH] = inputD[aktualni_pozice+(BLOCK_WIDTH-1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 2*BLOCK_WIDTH]; // horni podstava z==max
        krychlicka[BLOCK_WIDTH+1][ty_plus1][tx_plus1+3*BLOCK_WIDTH] = inputD[aktualni_pozice+(BLOCK_WIDTH-1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y + 3*BLOCK_WIDTH]; // horni podstava z==max

    }

    __syncthreads();

    // zapsani krychle 1
    outputD[aktualni_pozice] = (
                krychlicka[tz_plus1][ty_plus1][tx+2] +
                krychlicka[tz_plus1][ty_plus1][tx] +

                krychlicka[tz_plus1][ty+2][tx_plus1] +
                krychlicka[tz_plus1][ty][tx_plus1] +

                krychlicka[tz+2][ty_plus1][tx_plus1] +
                krychlicka[tz][ty_plus1][tx_plus1]
                ) * 0.1666666666f; // jakoze deleno 6


    // zapsani krychle 2
    tx += BLOCK_WIDTH;
    tx_plus1 += BLOCK_WIDTH;
    outputD[aktualni_pozice+BLOCK_WIDTH] = (
                krychlicka[tz_plus1][ty_plus1][tx+2] +
                krychlicka[tz_plus1][ty_plus1][tx] +

                krychlicka[tz_plus1][ty+2][tx_plus1] +
                krychlicka[tz_plus1][ty][tx_plus1] +

                krychlicka[tz+2][ty_plus1][tx_plus1] +
                krychlicka[tz][ty_plus1][tx_plus1]
                ) * 0.1666666666f; // jakoze deleno 6

    tx += BLOCK_WIDTH;
    tx_plus1 += BLOCK_WIDTH;
    // zapsani krychle 4
    outputD[aktualni_pozice+2*BLOCK_WIDTH] = (
                krychlicka[tz_plus1][ty_plus1][tx+2] +
                krychlicka[tz_plus1][ty_plus1][tx] +

                krychlicka[tz_plus1][ty+2][tx_plus1] +
                krychlicka[tz_plus1][ty][tx_plus1] +

                krychlicka[tz+2][ty_plus1][tx_plus1] +
                krychlicka[tz][ty_plus1][tx_plus1]
                ) * 0.1666666666f; // jakoze deleno 6

    tx += BLOCK_WIDTH;
    tx_plus1 += BLOCK_WIDTH;
    // zapsani krychle 3
    outputD[aktualni_pozice + 3*BLOCK_WIDTH] = (
                krychlicka[tz_plus1][ty_plus1][tx+2] +
                krychlicka[tz_plus1][ty_plus1][tx] +

                krychlicka[tz_plus1][ty+2][tx_plus1] +
                krychlicka[tz_plus1][ty][tx_plus1] +

                krychlicka[tz+2][ty_plus1][tx_plus1] +
                krychlicka[tz][ty_plus1][tx_plus1]
                ) * 0.1666666666f; // jakoze deleno 6

}


//=================================================
//                   KERNEL V3
//=================================================

// 16x16 threadu, nactou ve for cyklu vsech 16 pater krychle (14x14x14 pocitany bodu i okraje)
// 14x14 threadu spocita prumery uvnitr krychle = 64 z 256 threadu stoji
// pragma unroll, cca 0.05ms dolu
// velikost vstupni matice 268x268x268

#undef TOTAL_WIDTH
#define TOTAL_WIDTH 268
#undef BLOCK_WIDTH
#define BLOCK_WIDTH 14

void runKernel_prumer_V3(float *inputD, float *outputD)
{
    dim3 dimGrid(19,19,19);
    dim3 dimBlock(16,16,1);

    kernel_prumer_V3 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_prumer_V3(float *inputD, float *outputD)
{

    int ty = threadIdx.y;
    int tx = threadIdx.x;

    volatile __shared__ float krychlicka[16][16][16];

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH)*TOTAL_WIDTH*TOTAL_WIDTH) + // osa z
            ((blockIdx.y*BLOCK_WIDTH)*TOTAL_WIDTH) + // osa y
            (blockIdx.x*BLOCK_WIDTH);    // osa x

    // pozice aktualniho threadu ve vstupni i vystupni matici
    int aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH) +tx;

    // cyklus pres vsechny patra krychlicky
    for (signed int i=0; i< BLOCK_WIDTH+2; i++)
    {
        // kazdy thread nacita svoje
        krychlicka[i][ty][tx] = inputD[aktualni_pozice];

        // posun v ose Z o 1 nahoru
        aktualni_pozice += TOTAL_WIDTH*TOTAL_WIDTH;
    }

    __syncthreads();


    // vynechame okraje
    if ( (tx>0) && (tx<BLOCK_WIDTH+1) && (ty>0) && (ty<BLOCK_WIDTH+1))
    {
        // aktualni pozice zpet na zacatek (vynechame dolni podstavu)
        aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH) +tx + TOTAL_WIDTH*TOTAL_WIDTH;

        // for cyklus pres osu Z, pres patra 1-14
        #pragma unroll
        for (signed int i=1; i< (BLOCK_WIDTH+1); i++)
        {
            // kazdy thread nacita svoje
            outputD[aktualni_pozice] = (
                        krychlicka[i][ty][tx+1] +
                        krychlicka[i][ty][tx-1] +

                        krychlicka[i][ty+1][tx] +
                        krychlicka[i][ty-1][tx] +

                        krychlicka[i+1][ty][tx] +
                        krychlicka[i-1][ty][tx]
                        ) * 0.1666666666f; // jakoze deleno 6

            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH*TOTAL_WIDTH;
        }
    }
}

//=================================================
//                   KERNEL V3_1
//=================================================

// 16x16 threadu, nactou ve for cyklu vsech 16 pater krychle (14x14x14 pocitany bodu i okraje)
// 14x14 threadu spocita prumery uvnitr krychle = 64 z 256 threadu stoji
// velikost vstupni matice 268x268x268
// --
// pragma unroll neni, akorat zhorsilo
// oproti V3 eliminovana divergence pri zapiosvani, kdy okrajove 2 thready v ose x stoji
// bohuzel se to ale nevyplati
//   -- prepocet souradnic tak drahy (vyzaduje modulo a deleno)
//   -- vyuziti zpusobuje shared memory banks conficts => serializace pristupu do SM

// velikost vstupni matice 268x268x268

void runKernel_prumer_V3_1(float *inputD, float *outputD)
{
    dim3 dimGrid(19,19,19);
    dim3 dimBlock(16,16,1);

    kernel_prumer_V3_1 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_prumer_V3_1(float *inputD, float *outputD)
{

    int ty = threadIdx.y;
    int tx = threadIdx.x;

    volatile __shared__ float krychlicka[16][16][16];

    int pozice_blok = ((blockIdx.z*BLOCK_WIDTH)*TOTAL_WIDTH*TOTAL_WIDTH) + // osa z
            ((blockIdx.y*BLOCK_WIDTH)*TOTAL_WIDTH) + // osa y
            (blockIdx.x*BLOCK_WIDTH);    // osa x

    int aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH) +tx;

    for (signed int i=0; i< BLOCK_WIDTH+2; i++)
    {
        // kazdy thread nacita svoje
        krychlicka[i][ty][tx] = inputD[aktualni_pozice];

        // posun v ose Z o 1 nahoru
        aktualni_pozice += TOTAL_WIDTH*TOTAL_WIDTH;
    }

    __syncthreads();

    // pouzijeme pouze prvnich 196 threadu
    if ( (ty*16 + tx) < 196 )
    {

        int new_tx = ((ty*16 + tx) % 14) + 1;
        int new_ty = ((ty*16 + tx) / 14) + 1;

        // eliminace modula
        // int new_ty = ((ty*16 + tx) / 14) + 1;   // div
        // int new_tx = (ty*16 + tx) - 14*new_ty + 15;  // ((ty*16 + tx) modulo  14) +1

        // vynechame dolni podstavu
        aktualni_pozice = pozice_blok + (new_ty*TOTAL_WIDTH) +new_tx + TOTAL_WIDTH*TOTAL_WIDTH;

        // for cyklus pres osu Z
        for (signed int i=1; i< (BLOCK_WIDTH+1); i++)
        {
            outputD[aktualni_pozice] = (
                        krychlicka[i][new_ty][new_tx-1] +
                        krychlicka[i][new_ty][new_tx+1] +

                        krychlicka[i][new_ty+1][new_tx] +
                        krychlicka[i][new_ty-1][new_tx] +

                        krychlicka[i+1][new_ty][new_tx] +
                        krychlicka[i-1][new_ty][new_tx]

                        ) * 0.1666666666f; // jakoze deleno 6

            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH*TOTAL_WIDTH;
        }
    }
}

//=================================================
//                   KERNEL V4
//=================================================


// hybridni kernel
// 32x8 threadu postupne nactou 10(8 + 2 okraj) pater krychlicky
// 30x8 threadu potom spocita prumer
// 2 thready navic jsou pouze pro nacteni okraju v ose X
//  => pri vypoctu samotnem je tedy divergence threadu (2 z 32 stoji), ale lepsi nez sh. memory banks conflicts a serializace pristupu
// okraje v ose Y se nactou prevracenim 30x8 threadu

// velikost vstupni matice 258x258x272

#undef TOTAL_WIDTH_X
#define TOTAL_WIDTH_X 272
#undef TOTAL_WIDTH_Y
#define TOTAL_WIDTH_Y 258
#undef BLOCK_WIDTH_X
#define BLOCK_WIDTH_X 30
#undef BLOCK_WIDTH_Y
#define BLOCK_WIDTH_Y 8
#undef BLOCK_WIDTH_Z
#define BLOCK_WIDTH_Z 8

void runKernel_prumer_V4(float *inputD, float *outputD)
{
    dim3 dimGrid(9,32,32);
    dim3 dimBlock(32,8,1);

    kernel_prumer_V4 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_prumer_V4(float *inputD, float *outputD)
{
    int ty = threadIdx.y;
    int tx = threadIdx.x;

    volatile __shared__ float krychlicka[10][10][32];

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = (((blockIdx.z*BLOCK_WIDTH_Z))*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            (((blockIdx.y*BLOCK_WIDTH_Y)+1)*TOTAL_WIDTH_X) + // osa y
            (blockIdx.x*BLOCK_WIDTH_X);    // osa x

    int aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx;

    // nacteni samotne krychlicky + dolni(i==0) a horni(i==BLOCK_WIDTH_Z+2) podstavy
    // okraje pro X==0 a X==Max jsou reseny ze se zde nacita 32 bodu, ale prumer pocitame pro 30!

    for (signed int i=0; i< (BLOCK_WIDTH_Z+2); i++)
    {
        // kazdy thread nacita svoje
        krychlicka[i][ty+1][tx] = inputD[aktualni_pozice];

        // posun v ose Z o 1 nahoru
        aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
    }

    // nacteni okraje Y==0, prohodi se osy Y a Z
    aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) +tx;
    krychlicka[ty+1][0][tx] = inputD[aktualni_pozice];

    // nacteni okraje Y==BLOCK_WIDTH_Y+1, prohodi se osy Y a Z
    aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + (BLOCK_WIDTH_Y*TOTAL_WIDTH_X) +tx;
    krychlicka[ty+1][BLOCK_WIDTH_Y+1][tx] = inputD[aktualni_pozice];

    __syncthreads();

    // vynechame okraje
    if ((tx>0) && (tx<BLOCK_WIDTH_X+1))
    {
        // vynechame dolni podstavu
        aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) +tx  + TOTAL_WIDTH_X*TOTAL_WIDTH_Y;

        // for cyklus pres osu Z
        for (signed int i=1; i< (BLOCK_WIDTH_Z+1); i++)
        {
            // kazdy thread nacita svoje
            outputD[aktualni_pozice] = (
                        krychlicka[i][ty][tx+1] +
                        krychlicka[i][ty][tx-1] +

                        krychlicka[i][ty+1][tx] +
                        krychlicka[i][ty-1][tx] +

                        krychlicka[i+1][ty][tx] +
                        krychlicka[i-1][ty][tx]
                        ) * 0.1666666666f; // jakoze deleno 6

            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
        }
    }

}

//=================================================
//                   KERNEL V5
//=================================================

// nepouziva se sdilena pamet, primo se pocita
// velikost vstupni matice 285x258x258

#undef TOTAL_WIDTH_X
#define TOTAL_WIDTH_X 258
#undef TOTAL_WIDTH_Y
#define TOTAL_WIDTH_Y 258
#undef BLOCK_WIDTH_X
#define BLOCK_WIDTH_X 32
#undef BLOCK_WIDTH_Y
#define BLOCK_WIDTH_Y 8
#undef BLOCK_WIDTH_Z
#define BLOCK_WIDTH_Z 8

void runKernel_prumer_V5(float *inputD, float *outputD)
{
    dim3 dimGrid(8,32,32);
    dim3 dimBlock(32,8,1);

    // preferujeme L1 cache na ukor shared memory
    cudaFuncSetCacheConfig(kernel_prumer_V5,cudaFuncCachePreferL1);

    kernel_prumer_V5 <<<dimGrid,dimBlock>>>(inputD,outputD);
}

__global__ void kernel_prumer_V5(float *inputD, float *outputD)
{

    int ty = threadIdx.y;
    int tx = threadIdx.x;

    // souradnice, kde zacina (kde ma souradnice [0,0,0]) aktualni blok
    int pozice_blok = (((blockIdx.z*BLOCK_WIDTH_Z)+1)*TOTAL_WIDTH_X*TOTAL_WIDTH_Y) + // osa z
            (((blockIdx.y*BLOCK_WIDTH_Y)+1)*TOTAL_WIDTH_X) + // osa y
            ((blockIdx.x*BLOCK_WIDTH_X)+1);    // osa x

    // nacteni samotne krychlicky + dolni(i==0) a horni(i==BLOCK_WIDTH_Z+2) podstavy
    // okraje pro X==0 a X==Max jsou reseny ze se zde nacita 32 bodu, ale prumer pocitame pro 30!

    // promenne pro okoli bodu
    float x0, xMAX, y0, yMAX, z0, zMAX;

    int aktualni_pozice = pozice_blok + (ty*TOTAL_WIDTH_X) + tx;

    for (signed int k=0; k< (BLOCK_WIDTH_Z); k++)
    {

            // serazeno podle pravdepodobnosti ze se nachazi v L1 cache
            z0 =    inputD[aktualni_pozice - TOTAL_WIDTH_X*TOTAL_WIDTH_Y];
            y0 =    inputD[aktualni_pozice - TOTAL_WIDTH_X];
            x0 =    inputD[aktualni_pozice - 1];
            xMAX =  inputD[aktualni_pozice + 1];
            yMAX =  inputD[aktualni_pozice + TOTAL_WIDTH_X];
            zMAX =  inputD[aktualni_pozice + TOTAL_WIDTH_X*TOTAL_WIDTH_Y];

            // samotny vypocet
            outputD[aktualni_pozice] = (yMAX + zMAX + xMAX + z0 + y0 + x0) * 0.1666666666f; // jakoze deleno 6


            /*
            if ( (blockIdx.x==0) && (blockIdx.y==0) && (blockIdx.z==0) && (ty==0) && (tx==0))
            {
                printf("z0= %f y0=%f x0=%f  zMAX=%f yMAX=%f xMAX=%f\n]",z0,y0,x0,zMAX,yMAX,xMAX);
            }
            */


            // posun v ose Z o 1 nahoru
            aktualni_pozice += TOTAL_WIDTH_X*TOTAL_WIDTH_Y;
    }

}

/*
 * debugovaci kod - vypise obsah krychlicky v urcite threadu a blocku
 *
if ( (blockIdx.x==0) && (blockIdx.y==0) && (blockIdx.z==0) && (tz==0) && (ty==0) && (tx==0))
{
    for (int k=0; k<10 ;k++)
    {
        for (int j=0; j<10 ;j++)
        {
            for (int i=0; i<10 ;i++)
            {
                printf("%f\t",krychlicka[k][j][i]);
            }
            printf("\n");
        }
        printf("--------\n");
    }
}
*/

