/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#ifndef MERENIGPU_H_
#define MERENIGPU_H_

#include "hdf5_wrapper.h"

class MereniGPU
{
public:
    MereniGPU(hdf5_wrapper *HDF5);
    MereniGPU(hdf5_wrapper *HDF5_t0, hdf5_wrapper *HDF5_beta);
    ~MereniGPU();
    bool compareMatrixes(float *first, float *second);         // porovna 2 matice - dimenze bere z volajici instance

    void run(hdf5_wrapper *beta=NULL);                         // sdilene pro vsechny potomky, vola runAlgorithm

    float * dataOutputHost;             // 3D matice s vystupem 1 iterace algoritmu - v hostu
    float * dataOutputDevice;           // 3D matice s vystupem 1 iterace algoritmu - na karte
    float * dataBeta;                   // matice s promennou Beta z rovnice tepla
    //float * dataOutput2;              // pomocna 3D matice na prohazovani vstupu a vystupu
protected:
    float * createAndFillMatrix();      // alokuje matici a vyplni ji hodnotou -1
    bool checkMatrixBorders(float *matrix);  // vraci true, pokud je na hranici hodnota -1
    virtual void runAlgorithm();        // virtualni funkce, urcena pro dedeni - v potomcich je v ni samotny algoritmus

    float * dataInputHost;              // matice se vstupy - na hostu
    float * dataInputDevice;            // matice se vstupy - na karte
    int dimX;                           // rozmer dimenze X
    int dimY;                           // rozmer dimenze Y
    int dimZ;                           // rozmer dimenze Z
};

/*
 * nasleduji deklarace podedenych trid, kazda pro jeden kernel
 */


/*
 * ALGORITMY PRUMER
 */

class algoritmus_prumer_V1: public MereniGPU
{
public:
    algoritmus_prumer_V1(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V2: public MereniGPU
{
public:
    algoritmus_prumer_V2(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V2_1: public MereniGPU
{
public:
    algoritmus_prumer_V2_1(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V2_2: public MereniGPU
{
public:
    algoritmus_prumer_V2_2(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V3: public MereniGPU
{
public:
    algoritmus_prumer_V3(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V3_1: public MereniGPU
{
public:
    algoritmus_prumer_V3_1(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V4: public MereniGPU
{
public:
    algoritmus_prumer_V4(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_prumer_V5: public MereniGPU
{
public:
    algoritmus_prumer_V5(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};

/*
 * ALGORITMY HEAT
 */

class algoritmus_heat_V1: public MereniGPU
{
public:
    algoritmus_heat_V1(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};

class algoritmus_heat_V2_2: public MereniGPU
{
public:
    algoritmus_heat_V2_2(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};

class algoritmus_heat_V3: public MereniGPU
{
public:
    algoritmus_heat_V3(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};

class algoritmus_heat_V4_1: public MereniGPU
{
public:
    algoritmus_heat_V4_1(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_heat_V5: public MereniGPU
{
public:
    algoritmus_heat_V5(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};


class algoritmus_heat_V5_1: public MereniGPU
{
public:
    algoritmus_heat_V5_1(hdf5_wrapper *HDF5):MereniGPU(HDF5){}
private:
    void runAlgorithm();
};



#endif /* MERENIGPU_H_ */
