#ifndef DEFINES_H
#define DEFINES_H


// DELTA_T = deltaT / 12 ,
// deltaT=casovy krok simulace=100 mikrosekund;
// koeficient deltaT je v rovnici delen prave cislem 12
// 0.0001/8 = 0.000008333333
#define DELTA_T 0.0000083333f

// DELTA_X = 1 / ( (deltaX)^2 )
// deltaX=vzdalenost mezi dvema bodu v ose X, 1mm
// v rovnici je zni delana mocnina -> provede se dopredu
// suma okolnich bodu se deli (deltaX)^2, prevratime na efektivnijsi nasobeni pomoci 1/((deltaX)^2)
// obdobne pro osy Y a Z
#define DELTA_X 1000000.0f    // 1/(1mm^2) = 1/0.000001 = 1000000
#define DELTA_Y 1000000.0f    // 1/(1mm^2) = 1/0.000001 = 1000000
#define DELTA_Z 1000000.0f    // 1/(1mm^2) = 1/0.000001 = 1000000

// teplota tkane na okrajich prostredi
#define TEPLOTA_TELA 37.0f

#endif // DEFINES_H
