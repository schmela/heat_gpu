# Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
# Bakalarska prace, Sireni tepla na GPU

# Makefile pro GPU verzi simulace

CPP          = g++
CPPFLAGS      = -std=c++0x -pedantic -Wextra -g -O3 -march=corei7 -mtune=corei7 -msse

#HDF5
HDF_INSTALL = /usr/local/hdf5-1.8.10-serial
HDF_LIBS         = -L$(HDF_INSTALL)/lib $(HDF_INSTALL)/lib/libhdf5_hl.a  $(HDF_INSTALL)/lib/libhdf5_hl_cpp.a $(HDF_INSTALL)/lib/libhdf5_cpp.a $(HDF_INSTALL)/lib/libhdf5.a -lz -lrt -lm -Wl,-rpath -Wl,$(HDF_INSTALL)/lib
HDF_PARAMETRY = -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_BSD_SOURCE 
HDF_INCLUDE   = -I$(HDF_INSTALL)/include

#CUDA
NVCC = /usr/local/cuda-5.0/bin/nvcc
NVCC_PARAMS = -g -m64  -lineinfo -gencode arch=compute_20,code=sm_20
#NVCC_PARAMS = -m64  -lineinfo -gencode arch=compute_20,code=sm_20 --ptxas-options="-v -dlcm=cg"
CUDA_INCLUDE = -I/usr/local/cuda-5.0/include -I. -I.. -I../../common/inc
CUDA_LIBS = -m64 -L/usr/local/cuda-5.0/lib64 -lcudart


all: heat

heat: main.o hdf5_wrapper.o mereniGPU.o kernely_prumer.o kernely_heat.o
	$(CPP) $(CPPFLAGS) -o heat_gpu main.o hdf5_wrapper.o mereniGPU.o kernely_prumer.o kernely_heat.o $(HDF_LIBS) $(CUDA_LIBS)

main.o: main.cpp hdf5_wrapper.cpp hdf5_wrapper.h mereniGPU.cpp mereniGPU.h
	$(CPP) $(CPPFLAGS) $(HDF_INCLUDE) $(HDF_PARAMETRY) -c main.cpp

mereniGPU.o: mereniGPU.cpp mereniGPU.h kernely_prumer.cu kernely_prumer.h kernely_heat.cu kernely_heat.h hdf5_wrapper.cpp hdf5_wrapper.h 
	$(CPP) $(CPPFLAGS) $(CUDA_INCLUDE) $(HDF_INCLUDE) $(HDF_PARAMS) -c mereniGPU.cpp

kernely_prumer.o: kernely_prumer.cu kernely_prumer.h defines.h
	$(NVCC) $(NVCC_PARAMS) $(CUDA_INCLUDE) -c kernely_prumer.cu
	
kernely_heat.o: kernely_heat.cu kernely_heat.h defines.h
	$(NVCC) $(NVCC_PARAMS) $(CUDA_INCLUDE) -c kernely_heat.cu
	
hdf5_wrapper.o: hdf5_wrapper.cpp hdf5_wrapper.h
	$(CPP) $(CPPFLAGS) $(HDF_INCLUDE) $(HDF_PARAMETRY) -c hdf5_wrapper.cpp

clean:
	rm *.o heat_gpu

pack: 
	zip heat_gpu.zip Makefile *.cpp *.h *.cu
