/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#include <iostream>
#include <string>
#include <math.h>
#include <cuda_runtime.h>
#include <cuda_profiler_api.h>

#include "hdf5_wrapper.h"
#include "kernely_heat.h"
#include "kernely_prumer.h"
#include "mereniGPU.h"

using namespace std;

MereniGPU::~MereniGPU()
{
    delete dataOutputHost;
}

MereniGPU::MereniGPU(hdf5_wrapper *HDF5)
{
    dataInputHost = HDF5->inputMatrix;
    dimX = HDF5->dimX;
    dimY = HDF5->dimY;
    dimZ = HDF5->dimZ;

    // alokace a naplneni matice -1
    dataOutputHost = createAndFillMatrix();
}

MereniGPU::MereniGPU(hdf5_wrapper *HDF5, hdf5_wrapper *HDF5_beta)
{
    dataInputHost = HDF5->inputMatrix;
    dimX = HDF5->dimX;
    dimY = HDF5->dimY;
    dimZ = HDF5->dimZ;

    // alokace a naplneni matice -1
    dataOutputHost = createAndFillMatrix();
    dataBeta = NULL;
}

bool MereniGPU::compareMatrixes(float *first, float *second)
{
    bool stejne=true;

    for (int k=0; k< dimZ; k++)
        for (int j=0; j< dimY; j++)
            for (int i=0; i< dimX; i++)
            {
                int aktualni_pozice = k*dimY*dimX + j*dimX + i;

                // pokud jsou v obou maticich NaN, tak  se bunky preskoci
                if ((isnan(first[aktualni_pozice])) && (isnan(second[aktualni_pozice])))
                {
                    //cout << "NAN na pozici [" << j << "," << i << "]"<<endl;
                    continue;
                }

                // pokud jsou v obou maticich Inf, tak  se bunky preskoci
                if ((isinf(first[aktualni_pozice])) && (isinf(second[aktualni_pozice])))
                {
                    //cout << "INF na pozici [" << j << "," << i << "]"<<endl;
                    continue;
                }

                double prvni = first[aktualni_pozice];
                double druhe = second[aktualni_pozice];

                double buf = (double)prvni;
                long double delka = 1;

                // zjistime jak je cislo dlouhe pred desetinnou carkou
                while (fabs(buf) > 10.0f)
                {
                    buf /= 10.0;
                    delka *= 10;
                    //cout << "[" << k << "," << j << ","<< i << "]" <<  "buf: " << buf << " delka: " << delka << endl;
                }

                // prilis velka cisla ignorujeme
                //if (fabs(prvni) > 100000000000) continue;

                // dlouha cisla podelime aby se lepe porovnavala
                if (delka != 1)
                {
                    prvni /= (double)delka;
                    druhe /= (double)delka;
                }

                // kdyz je absolutni hodnota rozdilu 2 cisel vesti nez 0.001, vypise se chyba
                if (fabs(prvni - druhe) > 0.001f)
                {
                    cerr << "rozdil hodnot na pozici [" << k << "," << j << ","<< i << "] je  abs(" << prvni << " - " <<  druhe << ")=" << fabs(prvni - druhe) << endl;
                    cerr << "delka: " << delka << endl;
                    stejne = false;
                }


            }
    return stejne;
}

void MereniGPU::run(hdf5_wrapper *beta)
{

    // kopirovani textury do zarizeni
    cudaArray *cudaarray;
    if (beta != NULL)
    {
        dataBeta=beta->inputMatrix;
        cudaarray = prepareTexture(beta->inputMatrix,beta->dimX,beta->dimY,beta->dimZ);
    }

    int size = dimX*dimY*dimZ*sizeof(float);

    // alokace mista v zarizeni a kopirovani dat na kartu
    cudaMalloc((void **) &dataInputDevice, size);
    cudaMemcpy(dataInputDevice ,dataInputHost, size,cudaMemcpyHostToDevice);
    cudaMalloc((void **) &dataOutputDevice, size);
    cudaMemcpy(dataOutputDevice ,dataOutputHost, size,cudaMemcpyHostToDevice);

    // kolikrat probehne mereni
    int pocet_opakovani = 1;
    float timeValue, sumTimeValue=0.0f;

    // algoritmus mereni
    for (int i=0; i<pocet_opakovani; i++)
    {

        // udalost k mereni delky behu programu
        cudaEvent_t beginEvent;
        cudaEvent_t endEvent;

        cudaEventCreate( &beginEvent );
        cudaEventCreate( &endEvent );

        cudaEventRecord( beginEvent, 0 );

        cudaProfilerStart();

        runAlgorithm();

        cudaProfilerStop();

        cudaEventRecord( endEvent, 0 );
        cudaEventSynchronize( endEvent );

        cudaEventElapsedTime( &timeValue, beginEvent, endEvent );
        sumTimeValue += timeValue;

        cudaEventDestroy( beginEvent );
        cudaEventDestroy( endEvent );

    }

    if (beta != NULL)
    {
        freeTexture(cudaarray);
    }

    cout << endl << "Avg Time: " << sumTimeValue/pocet_opakovani << "ms" << endl;

    // kopie spocitanych dat z karty zpet
    cudaMemcpy(dataOutputHost ,dataOutputDevice, size,cudaMemcpyDeviceToHost);

    cudaFree(dataInputDevice);
    cudaFree(dataOutputDevice);

    cudaDeviceReset();


    /*
    // kontrola okraju na -1
    if ( !checkMatrixBorders(dataOutputHost))
        cerr << "na okraji neni -1" << endl;
     */
}

// funkce urcena pro dedeni - samotna by se nikdy nemela spustit
void MereniGPU::runAlgorithm()
{
    cerr << "Mereni::runAlgorithm - TOHLE SE NEMELO STAT" << std::endl;
}

float *MereniGPU::createAndFillMatrix()
{
    float *matrix = new float[dimX*dimY*dimZ];

    // vyplneni cele matice -1
    for (int k=0; k<dimZ;k++)
        for (int j=0; j<dimY;j++)
            for (int i=0; i<dimX;i++)
                matrix[k*dimY*dimX + j*dimX + i] = -1.0f;

    return matrix;
}

bool MereniGPU::checkMatrixBorders(float *matrix)
{
    // vzdy se zkontroluje uplne okoli

    // dolni podstava z=0
    for (int i= 0; i< dimX*dimY; i++)
        if (matrix[i] != -1.0f)
        {
            cerr << "Na hranici v dolni podstave neni -1. , v " << i << ". tem prvku je hodnota " << matrix[i] << endl;
            return false;
        }

    // horni podstava z=dimZ
    for (int i= (dimX*dimY)*(dimZ-1); i< dimX*dimY*dimZ; i++)
        if (matrix[i] != -1.0f)
        {
            cerr << "Na hranici v horni podstave neni -1. , v " << i << ". tem prvku je hodnota " << matrix[i] << endl;
            return false;
        }

    // x=0
    for (int k= 0; k< dimZ; k++)
        for (int j = 0;j < dimY; j++)
        if (matrix[k*dimX*dimY + j*dimX] != -1.0f)
        {
            cerr << "Na hranici, kdyz x=0 neni -1. [" << k << "," << j  << ",0] je hodnota " << matrix[k*dimX*dimY + j*dimX] << endl;
            return false;
        }

    // x=dimX
    for (int k= 0; k< dimZ; k++)
        for (int j = 0;j < dimY; j++)
        if (matrix[k*dimX*dimY + j*dimX + dimX - 1] != -1.0f)
        {
            cerr << "Na hranici, kdyz x=dimX neni -1. [" << k << "," << j  << ",dimX] je hodnota " << matrix[k*dimX*dimY + j*dimX + dimX - 1] << endl;
            return false;
        }

    // Y=0
    for (int k= 0; k< dimZ; k++)
        for (int i = 0;i < dimY; i++)
        if (matrix[k*dimX*dimY + i] != -1.0f)
        {
            cerr << "Na hranici, kdyz y=0 neni -1. [" << k << ", 0," << i  << "] je hodnota " << matrix[k*dimX*dimY + i] << endl;
            return false;
        }

    // Y=dimY
    for (int k= 0; k< dimZ; k++)
        for (int i = 0;i < dimY; i++)
            if (matrix[k*dimX*dimY + dimX*(dimY-1) + i] != -1.0f)
        {
            cerr << "Na hranici, kdyz y=dimY neni -1. [" << k << ", dimY," << i  << "] je hodnota " << matrix[k*dimX*dimY + dimX*(dimY-1) + i] << endl;
            return false;
        }

    // kdyz je pritomna i matice s betou (pocita se heat), tak se projede i vnitrnejsi okraj
    if (dataBeta != NULL)
    {
        // dolni podstava z=1
        for (int i=dimX*dimY ; i< 2*dimX*dimY; i++)
            if (matrix[i] != -1.0f)
            {
                cerr << "Na hranici v dolni podstave neni -1. , v " << i << ". tem prvku je hodnota " << matrix[i] << endl;
                return false;
            }

        // horni podstava z=dimZ-1
        for (int i= (dimX*dimY)*(dimZ-2); i< (dimX*dimY)*(dimZ-1); i++)
            if (matrix[i] != -1.0f)
            {
                cerr << "Na hranici v horni podstave neni -1. , v " << i << ". tem prvku je hodnota " << matrix[i] << endl;
                return false;
            }

        // x=1
        for (int k= 0; k< dimZ; k++)
            for (int j = 0;j < dimY; j++)
            if (matrix[k*dimX*dimY + j*dimX +1] != -1.0f)
            {
                cerr << "Na hranici, kdyz x=1 neni -1. [" << k << "," << j  << ",1] je hodnota " << matrix[k*dimX*dimY + j*dimX] << endl;
                return false;
            }

        // x=dimX-1
        for (int k= 0; k< dimZ; k++)
            for (int j = 0;j < dimY; j++)
            if (matrix[k*dimX*dimY + j*dimX + dimX - 2] != -1.0f)
            {
                cerr << "Na hranici, kdyz x=dimX-1 neni -1. [" << k << "," << j  << ",dimX-1] je hodnota " << matrix[k*dimX*dimY + j*dimX + dimX - 1] << endl;
                return false;
            }

        // Y=1
        for (int k= 0; k< dimZ; k++)
            for (int i = 0;i < dimY; i++)
            if (matrix[k*dimX*dimY + dimX + i] != -1.0f)
            {
                cerr << "Na hranici, kdyz y=1 neni -1. [" << k << ", 1," << i  << "] je hodnota " << matrix[k*dimX*dimY + i] << endl;
                return false;
            }

        // Y=dimY-1
        for (int k= 0; k< dimZ; k++)
            for (int i = 0;i < dimY; i++)
                if (matrix[k*dimX*dimY + dimX*(dimY-2) + i] != -1.0f)
            {
                cerr << "Na hranici, kdyz y=dimY-1 neni -1. [" << k << ", dimY-1," << i  << "] je hodnota " << matrix[k*dimX*dimY + dimX*(dimY-1) + i] << endl;
                return false;
            }
    }

    return true;
}

/*
 * nasleduji definice podedenych trid, kazda pro jeden kernel
 */

/*
 * ALGORITMY PRUMER
 */

void algoritmus_prumer_V1::runAlgorithm()
{
    runKernel_prumer_V1(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V2::runAlgorithm()
{
    runKernel_prumer_V2(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V2_1::runAlgorithm()
{
    runKernel_prumer_V2_1(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V2_2::runAlgorithm()
{
    runKernel_prumer_V2_2(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V3::runAlgorithm()
{
    runKernel_prumer_V3(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V3_1::runAlgorithm()
{
    runKernel_prumer_V3_1(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V4::runAlgorithm()
{
    runKernel_prumer_V4(dataInputDevice,dataOutputDevice);
}

void algoritmus_prumer_V5::runAlgorithm()
{
    runKernel_prumer_V5(dataInputDevice,dataOutputDevice);
}

/*
 * ALGORITMY HEAT
 */

void algoritmus_heat_V1::runAlgorithm()
{
    runKernel_heat_V1(dataInputDevice,dataOutputDevice);
}

void algoritmus_heat_V2_2::runAlgorithm()
{
    runKernel_heat_V2_2(dataInputDevice,dataOutputDevice);
}

void algoritmus_heat_V3::runAlgorithm()
{
    runKernel_heat_V3(dataInputDevice,dataOutputDevice);
}

void algoritmus_heat_V4_1::runAlgorithm()
{
    runKernel_heat_V4_1(dataInputDevice,dataOutputDevice);
}

void algoritmus_heat_V5::runAlgorithm()
{
    runKernel_heat_V5(dataInputDevice,dataOutputDevice);
}

void algoritmus_heat_V5_1::runAlgorithm()
{
    runKernel_heat_V5_1(dataInputDevice,dataOutputDevice);
}

