/*
    Bakalarska prace: Numericka distribuce tepla s vyuzitim GPU
    FIT VUT, 2012/13
    Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
*/

#include <iostream>
#include <string>

#include "hdf5_wrapper.h"
#include "mereniGPU.h"

using namespace std;



void testHeatVsechnyKernely()
{
    cout << "====================================" << endl;
    cout << "= TEST VSECH KERNELU - SIRENI TEPLA" << endl;
    cout << "====================================" << endl;

    hdf5_wrapper *HDF5_t0_260x260x260 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_260x260x284 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_260x260x272 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_268x268x268 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_256x256x256 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_256x256x280 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_264x264x264 = new (hdf5_wrapper);


    /*
    hdf5_wrapper *HDF5_matlab = new (hdf5_wrapper);
    HDF5_matlab->loadMatrixFromFile("output_matlab.h5","matrix");
    */

    HDF5_t0_260x260x260->loadMatrixFromFile("t0_260x260x260.h5","matrix");
    HDF5_t0_260x260x284->loadMatrixFromFile("t0_260x260x284.h5","matrix");
    HDF5_t0_260x260x272->loadMatrixFromFile("t0_260x260x272.h5","matrix");
    HDF5_t0_268x268x268->loadMatrixFromFile("t0_268x268x268.h5","matrix");

    HDF5_beta_256x256x256->loadMatrixFromFile("beta_256x256x256.h5","matrix");
    HDF5_beta_256x256x280->loadMatrixFromFile("beta_256x256x280.h5","matrix");
    HDF5_beta_264x264x264->loadMatrixFromFile("beta_264x264x264.h5","matrix");

    cout << endl << "GPU heat V1" << endl << "------------------";
    algoritmus_heat_V1 *algV1 = new algoritmus_heat_V1(HDF5_t0_260x260x260);
    algV1->run(HDF5_beta_256x256x256);
    HDF5_t0_260x260x260->writeMatrixToFile("output_V1.h5","matrix",algV1->dataOutputHost);
    delete algV1;

    cout << endl << "GPU heat V2_2" << endl << "------------------" << endl;
    algoritmus_heat_V2_2 *algV2_2 = new algoritmus_heat_V2_2(HDF5_t0_260x260x272);
    algV2_2->run(HDF5_beta_256x256x256);
    HDF5_t0_260x260x272->writeMatrixToFile("output_V2_2.h5","matrix",algV2_2->dataOutputHost);
    delete algV2_2;

    cout << endl << "GPU heat V3" << endl << "------------------";
    algoritmus_heat_V3 *algV3 = new algoritmus_heat_V3(HDF5_t0_268x268x268);
    algV3->run(HDF5_beta_264x264x264);
    HDF5_t0_268x268x268->writeMatrixToFile("output_V3.h5","matrix",algV3->dataOutputHost);
    delete algV3;

    cout << endl << "GPU heat V4_1" << endl << "------------------";
    algoritmus_heat_V4_1 *algV4_1 = new algoritmus_heat_V4_1(HDF5_t0_260x260x284);
    algV4_1->run(HDF5_beta_256x256x280);
    HDF5_t0_260x260x284->writeMatrixToFile("output_V4_1.h5","matrix",algV4_1->dataOutputHost);
    delete algV4_1;

    cout << endl << "GPU heat V5" << endl << "------------------";
    algoritmus_heat_V5 *algV5 = new algoritmus_heat_V5(HDF5_t0_260x260x260);
    algV5->run(HDF5_beta_256x256x256);
    HDF5_t0_260x260x260->writeMatrixToFile("output_V5.h5","matrix",algV5->dataOutputHost);
    delete algV5;

    cout << endl << "GPU heat V5_1" << endl << "------------------";
    algoritmus_heat_V5_1 *algV5_1 = new algoritmus_heat_V5_1(HDF5_beta_256x256x256);
    algV5_1->run(HDF5_beta_256x256x256);
    HDF5_beta_256x256x256->writeMatrixToFile("output_V5_1.h5","matrix",algV5_1->dataOutputHost);
    delete algV5_1;

/*
    if ( ! algV2_2->compareMatrixes(algV2_2->dataOutputHost,HDF5_matlab->inputMatrix))
    {
        cout << "Test V2_2 vs matlab:" << endl;
        std::cerr << "vysledne matice nejsou stejne" << std::endl;
    }
*/

/*
    if ( ! algV5_1->compareMatrixes(algV5_1->dataOutputHost,HDF5_matlab5->inputMatrix))
    {
        cout << "Test V5_1 vs V5_2:" << endl;
        std::cerr << "vysledne matice nejsou stejne" << std::endl;
    }
  */
    delete HDF5_beta_256x256x256;
    delete HDF5_beta_256x256x280;
    delete HDF5_beta_264x264x264;
    delete HDF5_t0_260x260x260;
    delete HDF5_t0_260x260x284;
    delete HDF5_t0_260x260x272;
    delete HDF5_t0_268x268x268;
}

// spusti test se vsemi kernely
void testPrumerVsechnyKernely()
{
    cout << "============================================" << endl;
    cout << "= TEST VSECH KERNELU - PRUMER ZE 6-ti OKOLI" << endl;
    cout << "============================================" << endl;

    hdf5_wrapper *HDF5_258x258x272 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_268x268x268 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_258x258x258 = new (hdf5_wrapper);

    HDF5_258x258x272->loadMatrixFromFile("t0_258x258x272.h5","matrix");
    HDF5_268x268x268->loadMatrixFromFile("t0_268x268x268.h5","matrix");
    HDF5_258x258x258->loadMatrixFromFile("t0_258x258x258.h5","matrix");

    cout << endl;

    cout << "GPU V1" << endl << "------------------";
    algoritmus_prumer_V1 *algpV1 = new algoritmus_prumer_V1(HDF5_258x258x258);
    algpV1->run();
    //HDF5_258x258x258->writeMatrixToFile("output_V1.h5","matrix",algV1->dataOutputHost);
    delete algpV1;

    cout << "GPU V2" << endl << "------------------";
    algoritmus_prumer_V2 *algpV2 = new algoritmus_prumer_V2(HDF5_258x258x258);
    algpV2->run();
    delete algpV2;

    cout << "GPU V2_1" << endl << "------------------";
    algoritmus_prumer_V2_1 *algpV2_1 = new algoritmus_prumer_V2_1(HDF5_258x258x272);
    algpV2_1->run();
    delete algpV2_1;

    cout << "GPU V2_2" << endl << "------------------";
    algoritmus_prumer_V2_2 *algpV2_2 = new algoritmus_prumer_V2_2(HDF5_258x258x272);
    algpV2_2->run();
    delete algpV2_2;

    cout << endl << "GPU V3" << endl << "------------------";
    algoritmus_prumer_V3 *algpV3 = new algoritmus_prumer_V3(HDF5_268x268x268);
    algpV3->run();
    delete algpV3;

    cout << endl << "GPU V3_1" << endl << "------------------";
    algoritmus_prumer_V3_1 *algpV3_1 = new algoritmus_prumer_V3_1(HDF5_268x268x268);
    algpV3_1->run();
    delete algpV3_1;

    cout << endl << "GPU V4" << endl << "------------------";
    algoritmus_prumer_V4 *algpV4 = new algoritmus_prumer_V4(HDF5_258x258x272);
    algpV4->run();
    delete algpV4;

    cout << endl << "GPU V5" << endl << "------------------";
    algoritmus_prumer_V5 *algpV5 = new algoritmus_prumer_V5(HDF5_258x258x258);
    algpV5->run();
    delete algpV5;

    delete HDF5_258x258x272;
    delete HDF5_268x268x268;
    delete HDF5_258x258x258;

}

// spusti kernel 5_1 pro velikosti 32^3, 64^3, 128^3, 256^3 a 512^3
// musi se zmenit velikosti bloku atd primo u kernelu pro kazdou velikost !!!
void testHeatV5_1_RuzneVelikostiVstupu()
{
    cout << "====================================" << endl;
    cout << "= TEST KERNELU V5_1 - SIRENI TEPLA" << endl;
    cout << "= ruzne velikosti vstupu" << endl;
    cout << "====================================" << endl;

    // definice vstupnich matic
    hdf5_wrapper *HDF5_beta_32x32x32 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_64x64x64 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_128x128x128 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_256x256x256 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_beta_256x512x512 = new (hdf5_wrapper);

    hdf5_wrapper *HDF5_t0_32x32x32 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_64x64x64 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_128x128x128 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_256x256x256 = new (hdf5_wrapper);
    hdf5_wrapper *HDF5_t0_256x512x512 = new (hdf5_wrapper);

    // nacteni vstupnich matic
    HDF5_beta_32x32x32->loadMatrixFromFile("beta_32x32x32.h5","matrix");
    HDF5_beta_64x64x64->loadMatrixFromFile("beta_64x64x64.h5","matrix");
    HDF5_beta_128x128x128->loadMatrixFromFile("beta_128x128x128.h5","matrix");
    HDF5_beta_256x256x256->loadMatrixFromFile("beta_256x256x256.h5","matrix");
    HDF5_beta_256x512x512->loadMatrixFromFile("beta_256x512x512.h5","matrix");

    HDF5_t0_32x32x32->loadMatrixFromFile("t0_32x32x32.h5","matrix");
    HDF5_t0_64x64x64->loadMatrixFromFile("t0_64x64x64.h5","matrix");
    HDF5_t0_128x128x128->loadMatrixFromFile("t0_128x128x128.h5","matrix");
    HDF5_t0_256x256x256->loadMatrixFromFile("t0_256x256x256.h5","matrix");
    HDF5_t0_256x512x512->loadMatrixFromFile("t0_256x512x512.h5","matrix");


    cout << "GPU heat V5_1 32" << endl << "------------------" << endl;
    algoritmus_heat_V5_1 *algV5_1 = new algoritmus_heat_V5_1(HDF5_t0_32x32x32);
    algV5_1->run(HDF5_beta_32x32x32);
    delete algV5_1;

    cout << "GPU heat V5_1 64" << endl << "------------------" << endl;
    algV5_1 = new algoritmus_heat_V5_1(HDF5_t0_64x64x64);
    algV5_1->run(HDF5_beta_64x64x64);
    delete algV5_1;

    cout << "GPU heat V5_1 128" << endl << "------------------" << endl;
    algV5_1 = new algoritmus_heat_V5_1(HDF5_t0_128x128x128);
    algV5_1->run(HDF5_beta_128x128x128);
    delete algV5_1;

    cout << "GPU heat V5_1 256" << endl << "------------------" << endl;
    algV5_1 = new algoritmus_heat_V5_1(HDF5_t0_256x256x256);
    algV5_1->run(HDF5_beta_256x256x256);
    delete algV5_1;

    cout << "GPU heat V5_1 512" << endl << "------------------" << endl;
    algV5_1 = new algoritmus_heat_V5_1(HDF5_t0_256x512x512);
    algV5_1->run(HDF5_beta_256x512x512);
    delete algV5_1;

    // destruktory vstupnich matic
    delete HDF5_beta_32x32x32;
    delete HDF5_beta_64x64x64;
    delete HDF5_beta_128x128x128;
    delete HDF5_beta_256x256x256;
    delete HDF5_beta_256x512x512;

    delete HDF5_t0_32x32x32;
    delete HDF5_t0_64x64x64;
    delete HDF5_t0_128x128x128;
    delete HDF5_t0_256x256x256;
    delete HDF5_t0_256x512x512;
}

/*               _
                (_)
 _ __ ___   __ _ _ _ __
| '_ ` _ \ / _` | | '_ \
| | | | | | (_| | | | | |
|_| |_| |_|\__,_|_|_| |_|

*/

int main()
{

    //testPrumerVsechnyKernely();
    testHeatVsechnyKernely();
    //testHeatV5_1_RuzneVelikostiVstupu();

    return 0;
}
