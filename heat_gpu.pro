TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    hdf5_wrapper.cpp \
    mereniGPU.cpp \
    kernels_prumer.cu \
    kernely_prumer.cu \
    kernely_heat.cu

HEADERS += \
    hdf5_wrapper.h \
    mereniGPU.h \
    kernels.h \
    kernely_prumer.h \
    kernely_heat.h \
    defines.h

OTHER_FILES +=

